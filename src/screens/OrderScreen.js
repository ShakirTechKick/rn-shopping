import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, BackHandler, SafeAreaView, StyleSheet, useWindowDimensions } from 'react-native';
import AppHeader from '../components/AppHeader';
import { Body, Title, Icon, Button } from 'native-base';
import { useStoreActions, useStoreState } from 'easy-peasy';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MyFavourites from './MyFavouritesScreen';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import moment from 'moment';
import useThemeColor from '../helpers/useThemeColor';

const styles = StyleSheet.create({
    content: {
        flex: 1
    },
    order: {
        padding: 10,
        borderBottomColor: 'rgba(50, 50, 50, 0.5)',
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
})

const Order = (props) => {

    const { color } = useThemeColor();
    const { width, height } = useWindowDimensions();
    const orders = useStoreState(state => state.orders.list);
    const clearOrders = useStoreActions(actions => actions.orders.clearOrders);


    const onPressOrder = (order) => {
        props.navigation.navigate('OrderDetailsView', { order })
    }


    const OrderNavigation = createMaterialBottomTabNavigator();

    const OrderView = ({ status }) => {

        var coloring = 'rgb(252, 186, 3)';

        if (status === 'COMPLETED') {
            coloring = 'green'
        }

        if (status === 'CANCELLED') {
            coloring = 'red';
        }

        const data = orders.filter(o => o.status === status);

        const renderOrder = ({ item: order, index }) => (
            <TouchableOpacity style={[styles.order, { backgroundColor: color.background }]} onPress={onPressOrder.bind(null, order)}>
                <View style={{ flex: 1 }}>
                    <Text style={{ fontWeight: 'bold' }}>{order.products[0].product}</Text>
                    <Text style={{ color: 'grey' }}>{order.products.length - 1} Other Products</Text>
                </View>
                <View>
                    <Text style={{ color: 'grey' }}>{moment(order.createdAt).format('YYYY MMM DD hh:mm A')}</Text>
                    <Text style={{ color: coloring, textAlign: 'right', fontWeight: 'bold' }}>{status}</Text>
                </View>
            </TouchableOpacity>
        )

        return (
            <FlatList data={data.sort((a, b) => moment(a.createdAt).diff(b.createdAt))} renderItem={renderOrder} keyExtractor={(item, index) => item.order_id} />
        )
    }

    const clearOrderList = () => {
        clearOrders()
    }

    return (
        <View style={{ flex: 1 }}>
            <AppHeader {...props}
                renderBodyComponent={() =>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                (<Body><Title>Order</Title></Body>)}
                renderRightComponent={() => (
                    <Button transparent>
                        <TouchableOpacity onPress={clearOrderList}>
                            <Text style={{ color: color.light }}>CLEAR LIST</Text>
                        </TouchableOpacity>
                    </Button>
                )}
            />

            <OrderNavigation.Navigator sceneAnimationEnabled barStyle={{ backgroundColor: color.dark }}>
                <OrderNavigation.Screen name='ONGOING' options={{
                    tabBarIcon: ({ focused, color }) => {
                        return <Icon name='directions-run' style={{ color: focused ? 'rgb(252, 186, 3)' : color, fontSize: 25 }} type='MaterialIcons' />
                    }
                }} component={OrderView.bind(null, { status: 'ONGOING' })} />

                <OrderNavigation.Screen name='COMPLETED' options={{
                    tabBarIcon: ({ focused, color }) => {
                        return <Icon name='check' style={{ color: focused ? 'green' : color, fontSize: 25 }} type='MaterialIcons' />
                    }
                }} component={OrderView.bind(null, { status: 'COMPLETED' })} />

                <OrderNavigation.Screen name='CANCELLED' options={{
                    tabBarIcon: ({ focused, color }) => {
                        return <Icon name='stop' style={{ color: focused ? 'red' : color, fontSize: 25 }} type='MaterialIcons' />
                    }
                }} component={OrderView.bind(null, { status: 'CANCELLED' })} />
            </OrderNavigation.Navigator>

        </View>
    );
}

Order.propTypes = {

};

export default Order;