import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, BackHandler, SafeAreaView, StyleSheet } from 'react-native';
import AppHeader from '../components/AppHeader';
import { Body, Title } from 'native-base';
import { useStoreActions } from 'easy-peasy';

const styles = StyleSheet.create({
    content: {
        padding: 15
    }
})

const About = (props) => {

    return (
        <SafeAreaView>
            <AppHeader  {...props} renderBodyComponent={() => (
                <Body><Title>About</Title></Body>
            )} />

            <View style={[styles.content]}>
                <Text>AboutScreen</Text>
            </View>
        </SafeAreaView>
    );
}

About.propTypes = {

};

export default About;