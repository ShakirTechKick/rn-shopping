import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, BackHandler, SafeAreaView, StyleSheet, useWindowDimensions, ImageBackground } from 'react-native';
import AppHeader from '../components/AppHeader';
import { Body, Title, Icon, Left, Right, Subtitle, Button, Grid, Row, Col } from 'native-base';
import { useStoreActions, useStoreState } from 'easy-peasy';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MyFavourites from './MyFavouritesScreen';
import { FlatList, TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import moment from 'moment';
import useThemeColor from '../helpers/useThemeColor';
import Payments from '../constants/Payment';

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    content: {
        flex: 1,
        padding: 10
    },
    section: {
        marginVertical: 15,
        borderColor: 'rgba(50, 50, 50, 0.5)',
        borderWidth: 0.5
    },
    header: {
        backgroundColor: 'rgba(50, 50, 50, 0.25)',
        padding: 10
    },
    heading: {
        fontWeight: 'bold'
    },
    view: {
        padding: 10
    },
    row: {
        marginVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    block: {
        margin: 10
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomWidth: 0.5,
        padding: 10
    },
    body: {
        padding: 10
    },
    button: {
        padding: 10,
        borderRadius: 5
    }
})

const OrderDetailScreen = (props) => {

    const { color } = useThemeColor();

    const { width, height } = useWindowDimensions();

    const categories = useStoreState(state => state.categories.list);
    const { order } = props.route.params;


    const onViewTracking = () => {
        props.navigation.navigate('OrderTrackView', { order });
    }

    return (
        <View style={styles.container}>
            <AppHeader {...props} renderBodyComponent={() => (
                <Body>
                    <Title>Order Details</Title>
                    <Subtitle>{order.products[0].product}</Subtitle>
                </Body>)}
                renderLeftComponent={() => (
                    <Left>
                        <TouchableOpacity onPress={() => props.navigation.pop()}>
                            <Icon name='keyboard-arrow-left' style={{ color: color.light }} type='MaterialIcons' />
                        </TouchableOpacity>
                    </Left>
                )} renderRightComponent={() => (
                    <Right>
                        <TouchableOpacity onPress={onViewTracking} style={[styles.button, { backgroundColor: color.light }]}>
                            <Text style={{ fontWeight: 'bold' }}>Track Order</Text>
                        </TouchableOpacity>
                    </Right>
                )} />

            <ScrollView style={styles.content}>
                <View style={styles.section}>
                    <View style={[styles.header, { borderBottomColor: color.dark }]}>
                        <Text style={styles.heading}>Shipping Method</Text>
                    </View>
                    <View style={styles.view}>
                        <Text>{order.shipping_method ? order.shipping_method : null}</Text>
                    </View>
                </View>

                <View style={styles.section}>
                    <View style={[styles.header, { borderBottomColor: color.dark }]}>
                        <Text style={styles.heading}>Products</Text>
                    </View>
                    <View style={[styles.view, { padding: 0 }]}>

                        {
                            order.products.map((product, index) => (
                                <View style={[styles.block, { backgroundColor: color.light }]}>
                                    <View style={[styles.header, { borderBottomColor: color.dark }]}>
                                        <ImageBackground
                                            resizeMode='cover'
                                            style={{ width: 100, height: 50, marginEnd: 5 }}
                                            source={{ uri: product.images[0] }} />
                                        <View>
                                            <Text style={{ fontWeight: 'bold' }}>{product.product}</Text>
                                            <Text style={{ color: 'grey' }}>
                                                {categories.find(c => c.category_id === product.category_id) ?
                                                    categories.find(c => c.category_id === product.category_id).category : null}
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={styles.body}>
                                        <Grid>
                                            <Row style={{ marginVertical: 5 }}>
                                                <Col><Text style={{ fontWeight: 'bold' }}>Product Name</Text></Col>
                                                <Col><Text style={{ textAlign: 'right' }}>{product.product}</Text></Col>
                                            </Row>
                                            <Row style={{ marginVertical: 5 }}>
                                                <Col><Text style={{ fontWeight: 'bold' }}>Price</Text></Col>
                                                <Col><Text style={{ textAlign: 'right' }}>{product.price}</Text></Col>
                                            </Row>
                                            <Row style={{ marginVertical: 5 }}>
                                                <Col><Text style={{ fontWeight: 'bold' }}>Quantity</Text></Col>
                                                <Col><Text style={{ textAlign: 'right' }}>{product.quantity}</Text></Col>
                                            </Row>
                                            {
                                                product.selections.map((selection, index) => (
                                                    <Row style={{ marginVertical: 5 }}>
                                                        <Col><Text style={{ fontWeight: 'bold' }}>{selection.name}</Text></Col>
                                                        <Col>
                                                            <Text style={{ textAlign: 'right' }}>
                                                                {selection.current.split('-').pop()}
                                                                {!!selection.values.find(period => period.value === selection.current.split('-').pop()).additional &&
                                                                    (`(+${selection.values.find(period => period.value === selection.current.split('-').pop()).additional})`)}
                                                            </Text>
                                                        </Col>
                                                    </Row>
                                                ))
                                            }
                                        </Grid>
                                    </View>
                                </View>
                            ))
                        }

                    </View>
                </View>

                <View style={styles.section}>
                    <View style={[styles.header, { borderBottomColor: color.dark }]}>
                        <Text style={styles.heading}>Sub Total</Text>
                    </View>
                    <View style={styles.view}>
                        <View style={styles.row}>
                            <Text>Sub Total</Text>
                            <Text style={{ textAlign: 'right' }}>{order.subtotal}</Text>
                        </View>

                        <View style={styles.row}>
                            <Text>Tax</Text>
                            <Text style={{ textAlign: 'right' }}>{order.tax}</Text>
                        </View>

                        <View style={styles.row}>
                            <Text>Shipping</Text>
                            <Text style={{ textAlign: 'right' }}>{order.shipping}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text>Discount</Text>
                            <Text style={{ textAlign: 'right' }}>{order.discount}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text>Total</Text>
                            <Text style={{ textAlign: 'right' }}>{order.grandtotal}</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.section}>
                    <View style={[styles.header, { borderBottomColor: color.dark }]}>
                        <Text style={styles.heading}>Payment Method</Text>
                    </View>
                    <View style={styles.view}>
                        <Text>{Payments[order.payment_method]}</Text>
                    </View>
                </View>
            </ScrollView>

        </View>
    );
}

OrderDetailScreen.propTypes = {

};

export default OrderDetailScreen;