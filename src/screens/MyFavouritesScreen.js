import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, BackHandler, SafeAreaView, StyleSheet } from 'react-native';
import AppHeader from '../components/AppHeader';
import { Body, Title } from 'native-base';
import { useStoreState } from 'easy-peasy';
import Product from '../components/Product';
import { FlatList, ScrollView } from 'react-native-gesture-handler';

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})

const MyFavourites = (props) => {

    const favourites = useStoreState(state => state.favourites.list);

    const renderFavourites = ({ item, index }) => {
        return (
            <Product navigation={props.navigation} key={item.product_id}
                product={item}
                isAddedToCart={item.isAddedToCart}
                isFavourite={favourites.find(f => f.product_id === item.product_id)} />
        )
    }

    return (
        <View style={styles.container}>
            <AppHeader {...props} renderBodyComponent={() => (
                <Body><Title>My Favourites</Title></Body>)} />

            <ScrollView style={{ flex: 1 }}>
                <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start' }}>
                    {
                        favourites.map((product, index) => (
                            renderFavourites({ item: product, index })
                        ))
                    }
                </View>
            </ScrollView>
        </View>
    );
}

MyFavourites.propTypes = {

};

export default MyFavourites;