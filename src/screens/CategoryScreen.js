import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, BackHandler, SafeAreaView, StyleSheet, ImageBackground } from 'react-native';
import AppHeader from '../components/AppHeader';
import { Body, Title } from 'native-base';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { useStoreState } from 'easy-peasy';
import useThemeColor from '../helpers/useThemeColor';

const styles = StyleSheet.create({
    content: {
        padding: 15
    },
    category: {
        flex: 1,
        marginVertical: 10,
        height: 75,
        borderRadius: 5,
        flexDirection: 'row', padding: 5,
        alignItems: 'center', justifyContent: 'flex-start'
    },
    header: {
        fontWeight: 'bold',
        fontSize: 15, marginBottom: 5
    },
    products: {
        fontSize: 10,
        color: 'grey'
    }
})

const Category = (props) => {

    const { color } = useThemeColor();

    const categories = useStoreState(state => state.categories.list);
    const products = useStoreState(state => state.products.list);

    const navigateToShopView = item => {
        props.navigation.navigate('ShopView', { item, heading: 'Categories' });
    }

    const renderCategories = ({ item, index }) => {
        return (
            <TouchableOpacity onPress={navigateToShopView.bind(null, item)} style={[styles.category, { backgroundColor: color.light }]}>
                <ImageBackground source={{ uri: item.image }}
                    resizeMode='contain'
                    style={{ width: 50, height: 50, borderRadius: 50 }} />
                <View style={{ marginHorizontal: 15 }}>
                    <Text style={styles.header}>{item.category}</Text>
                    <Text style={styles.products}>
                        ({products && products.filter(p => p.category_id === item.category_id).length} Products)
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }

    return (
        <SafeAreaView>
            <AppHeader  {...props} renderBodyComponent={() => (
                <Body><Title>Categories</Title></Body>
            )} />

            <View style={[styles.content]}>
                <FlatList
                    data={categories} renderItem={renderCategories} keyExtractor={(item, index) => item.category_id} />
            </View>
        </SafeAreaView>
    );
}

Category.propTypes = {

};

export default Category;