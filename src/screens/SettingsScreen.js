import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, BackHandler, SafeAreaView, StyleSheet } from 'react-native';
import AppHeader from '../components/AppHeader';
import { Body, Title } from 'native-base';
import { Switch } from 'react-native-paper';
import { useStoreState, useStoreActions } from 'easy-peasy';
import useThemeColor from '../helpers/useThemeColor';
import { TouchableOpacity } from 'react-native-gesture-handler';

const styles = StyleSheet.create({
    content: {
        padding: 15
    },
    section: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'rgba(50, 50, 50, 0.25)',
        borderRadius: 5, padding: 10,
        marginBottom: 10
    }
})

const Setting = (props) => {

    const { color } = useThemeColor();

    const { setThemeCurrent } = useStoreActions(actions => actions.app);
    const { current } = useStoreState(state => state.app);
    const { user } = useStoreState(state => state.auth);

    const onDarkThemeChanged = (checked) => {
        setThemeCurrent(checked ? 'Dark' : 'Light');
    }

    const viewAccount = () => {
        if (user) {
            props.navigation.navigate('AccountView');
        } else {
            props.navigation.navigate('AuthScreen');
        }
    }

    return (
        <SafeAreaView>
            <AppHeader  {...props} renderBodyComponent={() => (
                <Body><Title>Settings</Title></Body>
            )} />

            <View style={[styles.content]}>
                <TouchableOpacity onPress={viewAccount} style={styles.section}>
                    <View>
                        <Text style={{ fontWeight: 'bold' }}>Account</Text>
                        <Text style={{ fontSize: 10 }}>{user ? user.email : 'Please Log in'}</Text>
                    </View>
                    <Text style={{ fontSize: 12 }}>{user ? 'Logged' : 'Not Logged'}</Text>
                </TouchableOpacity>

                <View style={styles.section}>
                    <Text style={{ fontWeight: 'bold' }}>Dark Modes</Text>
                    <Switch color={color.dark} value={current == 'Dark'}
                        onValueChange={onDarkThemeChanged} />
                </View>

            </View>
        </SafeAreaView>
    );
}

Setting.propTypes = {

};

export default Setting;