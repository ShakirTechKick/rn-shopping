import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, BackHandler, SafeAreaView, StyleSheet, useWindowDimensions, ImageBackground } from 'react-native';
import AppHeader from '../components/AppHeader';
import { Body, Title, Icon, Left, Right, Subtitle, Button, Grid, Row, Col } from 'native-base';
import { useStoreActions, useStoreState } from 'easy-peasy';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MyFavourites from './MyFavouritesScreen';
import { FlatList, TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import moment from 'moment';
import useThemeColor from '../helpers/useThemeColor';

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    content: {
        flex: 1,
        padding: 10
    },
    section: {
        marginVertical: 15,
        borderColor: 'rgba(50, 50, 50, 0.5)',
        borderWidth: 0.5
    },
    header: {
        backgroundColor: 'rgba(50, 50, 50, 0.25)',
        padding: 10
    },
    heading: {
        fontWeight: 'bold'
    },
    view: {
        padding: 10
    },
    row: {
        marginVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    block: {
        margin: 10
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomWidth: 0.5,
        padding: 10
    },
    body: {
        padding: 10
    },
    button: {
        padding: 10,
        borderRadius: 5
    },
    pan: {
        flex: 1,
        borderLeftColor: 'black',
        borderLeftWidth: 2,
        borderStyle: 'dashed',
        borderRadius: 5,
        marginVertical: 25
    },
    track: {
        padding: 10,
        borderRadius: 5,
        flexDirection: 'row', alignItems: 'center',
        justifyContent: 'space-between',
        width: 250
    },
    connector: {
        height: 75,
        // alignSelf: 'center',
        left: '60%',
        width: 1.5, backgroundColor: 'rgba(50, 50, 50, 0.25)'
    },
    vconnect: {
        width: 75, height: 2,
        backgroundColor: 'rgba(50, 50, 50, 0.5)'
    },
    trackholder: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    indicator: {
        width: 30, height: 30,
        backgroundColor: 'red',
        justifyContent: 'center', alignItems: 'center', borderRadius: 50,
        position: 'absolute', left: 10, top: -15
    }
})

const OrderTracking = (props) => {

    const { color } = useThemeColor();

    const { width, height } = useWindowDimensions();

    const orders = useStoreState(state => state.orders.list);
    const { order: { order_id } } = props.route.params;

    const [order, setOrder] = React.useState(orders.find(o => o.order_id === order_id));

    React.useEffect(() => {
        setOrder(orders.find(o => o.order_id === order_id));
    }, [orders])

    const getStatusColor = (status) => {
        var color = 'rgb(0, 186, 50)';
        switch (status) {
            case 'COMPLETED':
                color = 'green';
                break;
            case 'CANCELLED':
                color = 'red';
                break;
            default: color = 'rgb(50, 0, 75)';
        }

        return color;
    }

    return (
        <View style={styles.container}>
            <AppHeader {...props} renderBodyComponent={() => (
                <Body>
                    <Title>Track Order</Title>
                    <Subtitle>{order.products[0].product}</Subtitle>
                </Body>)}
                renderLeftComponent={() => (
                    <Left>
                        <TouchableOpacity onPress={() => props.navigation.pop()}>
                            <Icon name='keyboard-arrow-left' style={{ color: color.light }} type='MaterialIcons' />
                        </TouchableOpacity>
                    </Left>
                )}
                renderRightComponent={() => (
                    <Right>
                        <Text style={{ color: color.light, fontWeight: 'bold' }}>
                            {order.tracking[order.tracking.length - 1].status}
                        </Text>
                    </Right>
                )} />

            <ScrollView style={styles.content}>
                <View style={[styles.pan]}>
                    {
                        order.tracking.map((track, index) => (
                            <>
                                <View style={styles.trackholder}>
                                    <View style={styles.vconnect}>
                                        <View style={styles.indicator}>
                                            <Text style={{ color: color.light }}>{index + 1}</Text>
                                        </View>
                                    </View>
                                    <View key={index} style={[styles.track, { backgroundColor: 'rgb(200, 200,200)' }]}>
                                        <View>
                                            <Text style={{ fontSize: 15, fontWeight: 'bold' }}>{track.activity}</Text>
                                            <Text style={{ fontSize: 12, color: 'grey' }}>{
                                                moment(track.timestamp).format('hh:mm:ss A Do MMM YYYY')
                                            }</Text>
                                        </View>
                                    </View>
                                </View>
                                {(index < order.tracking.length - 1) && <View style={styles.connector}></View>}
                            </>
                        ))
                    }
                </View>
            </ScrollView>

        </View>
    );
}

OrderTracking.propTypes = {

};

export default OrderTracking;