import React from 'react'
import { View, Text, StyleSheet, TextInput, useWindowDimensions, StatusBar } from 'react-native';
import { ActivityIndicator, TextInput as TextInputPaper, Snackbar } from 'react-native-paper';
import CountryPicker from 'react-native-country-picker-modal';
import AnimatedView from '../components/AnimatedView';
import { Icon, Button, Body, Right, Left } from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AppHeader from '../components/AppHeader';
import useThemeColor from '../helpers/useThemeColor';
import LoadingDialog from '../components/LoadingDialog';
import { Request, SourceRequest } from '../services/Request';
import { useStoreActions, useStoreState } from 'easy-peasy';
import Axios from 'axios';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15,
        justifyContent: 'center', alignItems: 'center'
    },
    form: {
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 5,
        padding: 5, width: '75%'
    },
    header: {
        fontSize: 25,
        marginVertical: 50
    },
    button: {
        marginVertical: 50,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        borderRadius: 50
    },
    back: { flexDirection: 'row', alignItems: 'center' }
})

const LoginScreen = ({ route, navigation }) => {

    const [progress, setProgress] = React.useState(false);

    const { setUser } = useStoreActions(actions => actions.auth);
    const { token } = useStoreState(state => state.app);

    var history = null;
    var payload = {};
    if (route.params) {
        history = route.params.history ? route.params.history : null;
        payload = route.params.payload ? route.params.payload : {};
    }

    const { color } = useThemeColor();

    const [snack, setSnack] = React.useState({ visibility: 'closed', body: null });

    const [number, setNumber] = React.useState(null);
    const [countryCodeSelected, setCountryCodeSelected] = React.useState('LK');
    const [callingCodeSelected, setCallingCodeSelected] = React.useState('94');
    const [country, setCountry] = React.useState(null);

    const { width, height } = useWindowDimensions();

    const onSelectCountry = (country) => {
        setCountryCodeSelected(country.cca2);
        setCallingCodeSelected(country.callingCode);
        setCountry(country);
    }



    const onBackPress = () => {
        navigation.pop();
    }

    const onPressNext = () => {
        if (number && number.length > 9) {

            const customer = (`+${callingCodeSelected + number}`);

            setProgress(true);

            Request.post('authentication', { number: customer, token })
                .then(response => {
                    if (response.status === 200) {

                        const { user, user_status, pin } = response.data;

                        setUser({ ...user, status: user_status, pin });

                        setProgress(null);

                        navigation.navigate('VerifyView', {
                            number: customer, otp: pin, history, payload
                        })

                    } else {
                        setProgress(null);
                    }
                }).catch(error => {
                    setProgress(null);
                })

        } else {
            setSnack({ visibility: 'open', body: 'Number must be length of minimum 10' })
        }
    }

    const setPhoneNumber = (num) => {
        setNumber(n => num);
    }

    return (
        <>
            <AppHeader transparent headerBackgroundColor={color.dark} statusBarColor={'transparent'} renderLeftComponent={() => (
                <Left>
                    <TouchableOpacity style={styles.back} onPress={onBackPress}>
                        <Icon name='keyboard-arrow-left' style={{ color: color.light }} type='MaterialIcons' />
                        <Text style={{ color: color.light, marginBottom: 2 }}>Back</Text>
                    </TouchableOpacity>
                </Left>
            )} renderBodyComponent={() => <Body />} renderRightComponent={() => <Right />} />
            <View style={[styles.container, { backgroundColor: color.dark }]}>
                <View style={styles.header}>
                    <Text style={{ color: color.light, fontSize: 25, fontWeight: 'bold' }}>Enter Your Number</Text>
                </View>
                <View style={[styles.form, {
                    backgroundColor: color.light
                }]}>
                    <CountryPicker
                        countryCode={countryCodeSelected}
                        withEmoji withCallingCode withAlphaFilter
                        flatListProps={{
                            removeClippedSubviews: true,
                            maxToRenderPerBatch: height / 50, windowSize: 21,
                            getItemLayout: (data, index) => ({ length: 50, offset: index * 50, index })
                        }} onSelect={onSelectCountry} />
                    <Text>+{callingCodeSelected}</Text>
                    <View>
                        <TextInput underlineColorAndroid='transparent'
                            placeholder='0776905859' maxLength={15} selectionColor={color.dark} keyboardType='phone-pad' onChangeText={setPhoneNumber}
                            style={{
                                borderBottomColor: 'transparent',
                                padding: 10, marginHorizontal: 5
                            }} />
                    </View>
                </View>
                <AnimatedView styles={[styles.button, { backgroundColor: color.light }]} action={onPressNext}>
                    <Icon name='keyboard-arrow-right' type='MaterialIcons' />
                </AnimatedView>
            </View>

            <Snackbar visible={snack.visibility === 'open'} onDismiss={() => setSnack({ ...snack, visibility: 'closed' })}>
                {snack.body}
            </Snackbar>
            <LoadingDialog dismissable={false} progress={progress} />
        </>
    )

}

export default LoginScreen;
