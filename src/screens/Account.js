import React from 'react'
import { View, StyleSheet, TextInput, Text, ImageBackground, KeyboardAvoidingView, Platform, PermissionsAndroid, Alert } from 'react-native';
import { useStoreActions, useStoreState } from 'easy-peasy';
import useThemeColor from '../helpers/useThemeColor';
import AppHeader from '../components/AppHeader';
import { Body, Icon, Left, Right, Title } from 'native-base';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import FastImageGallery from 'react-native-fast-image';
import { Request, FormRequest } from '../services/Request';
import { Dialog, Snackbar } from 'react-native-paper';
import LoadingDialog from '../components/LoadingDialog';
import ImagePicker from 'react-native-image-crop-picker';

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    content: { flex: 1 },
    row: {
        margin: 10,
        width: '90%'
    },
    label: {
        marginVertical: 5,
        fontWeight: 'bold'
    },
    input: { borderBottomColor: 'transparent', borderBottomWidth: 0 },
    avatar: {
        width: '100%',
        alignItems: 'center'
    },
    photo: {
        width: '100%',
        height: 200,
        backgroundColor: 'black',
        marginHorizontal: 25
    },
    gallery: {
        position: 'absolute', bottom: 25, right: 25
    },
    gallery__button: {
        width: 50,
        height: 50, backgroundColor: 'rgba(250, 250, 250, 0.75)',
        justifyContent: 'center', alignItems: 'center', borderRadius: 50
    },
    button: {
        width: '100%'
    },
    button__click: { padding: 15 },
    dialog__button: {
        width: '100%',
        padding: 15
    }
})

function Account(props) {

    const { color } = useThemeColor();
    const { setUser } = useStoreActions(actions => actions.auth);
    const { user } = useStoreState(state => state.auth);

    const [cameraDialog, setCameraDialog] = React.useState('closed');

    const [snack, setSnack] = React.useState({ visibility: 'closed', body: null });
    const [progress, setProgress] = React.useState(false);

    const [avatar, setAvatar] = React.useState({ uri: user?.avatar });

    const [form, setForm] = React.useState({
        username: user?.username,
        email: user?.email
    })

    const saveAccount = () => {

        setProgress(true);

        Request.post('profile', {
            user_id: user.user_id,
            ...form
        }).then(response => {
            if (response.status === 200) {
                setUser({ ...user, ...form });
                setSnack({ visibility: 'open', body: 'Successfully account updated' });
            } else {
                setSnack({ visibility: 'open', body: 'Error updating account' });
            }
        }).catch(error => {
            setSnack({ visibility: 'open', body: 'Error updating account' });
        }).finally(() => {
            setProgress(false);
        })

    }

    const updateProfilePhoto = (resource) => {
        const source = { uri: resource.path };
        setAvatar(source);
        setCameraDialog('closed');
        setProgress(true);

        const fd = new FormData();
        fd.append('img', { name: 'avatar.jpg', type: resource.mime, uri: resource.path });
        fd.append('user_id', user.user_id);

        FormRequest.post('profileimg', fd).then(response => {
            if (response.status === 200) {
                const uri = response.data.img;
                setAvatar({ uri });
                setUser({ ...user, avatar: uri });
                setSnack({ visibility: 'open', body: 'Successfully updated avatar' });
            } else {
                setSnack({ visibility: 'open', body: 'Error updating avatar' });
            }
        }).catch(error => {
            setSnack({ visibility: 'open', body: 'Error updating avatar' });
        }).finally(() => {
            setProgress(false);
        })


    }

    const openImagePicker = () => {

        ImagePicker.openPicker({
            cropping: true
        }).then(output => {
            if (output.path) {
                updateProfilePhoto(output)
            }
        }).catch(error => {
            if (!(error == 'Error: User cancelled image selection')) {
                setSnack({ visibility: 'open', body: 'Error handling gallery' });
            }
        })
    }

    const openCameraPicker = () => {

        ImagePicker.openCamera({
            cropping: true
        }).then(output => {
            if (output.path) {
                updateProfilePhoto(output)
            }
        }).catch(error => {

            if (!(error == 'Error: User cancelled image selection')) {
                setSnack({ visibility: 'open', body: 'Error handling camera' });
            }

        })
    }

    const triggerImagePicker = async (type) => {

        if (Platform.Version >= 23) {


            try {

                const WritePermission = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
                const CameraPermission = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA);

                if (WritePermission && CameraPermission) {
                    if (type === 'Camera') {
                        openCameraPicker();
                    } else {
                        openImagePicker();
                    }
                } else {


                    PermissionsAndroid.requestMultiple(
                        [
                            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                            PermissionsAndroid.PERMISSIONS.CAMERA],
                        {
                            'title': 'Gallery',
                            'message': 'LinkChat needs access to your gallery and camera.'
                        }
                    ).then(() => {

                        if (type === 'Camera') {
                            openCameraPicker();
                        } else {
                            openImagePicker();
                        }

                    }).catch(err => {
                        setSnack({ visibility: 'open', body: 'No permission granted' });
                    })

                }

            } catch (error) {
                setSnack({ visibility: 'open', body: 'Error happened' });
            }

        } else {
            try {

                const WritePermission = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
                const CameraPermission = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA);

                if (WritePermission && CameraPermission) {
                    if (type === 'Camera') {
                        openCameraPicker();
                    } else {
                        openImagePicker();
                    }
                } else {
                    setSnack({ visibility: 'open', body: 'No permission granted' });
                }
            } catch (error) {
                setSnack({ visibility: 'open', body: 'Error happened' });
            }
        }
    }

    return (
        <View style={styles.container}>
            <AppHeader {...props} renderBodyComponent={() => (
                <Body><Title>Account</Title></Body>)}
                renderLeftComponent={() => (
                    <Left>
                        <TouchableOpacity onPress={() => props.navigation.pop()}>
                            <Icon name='keyboard-arrow-left' style={{ color: color.light }} type='MaterialIcons' />
                        </TouchableOpacity>
                    </Left>
                )}
                renderRightComponent={() => (<Right></Right>)} />


            <ScrollView style={styles.content}>
                <View style={styles.avatar}>
                    <ImageBackground source={avatar} style={styles.photo}>
                        <View style={styles.gallery}>
                            <TouchableOpacity onPress={() => setCameraDialog('open')} style={styles.gallery__button}>
                                <Icon type='MaterialIcons' name='camera' />
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                </View>

                <View style={styles.row}>
                    <Text style={styles.label}>Name</Text>
                    <View>
                        <TextInput defaultValue={form.username}
                            style={styles.input} onChangeText={username => setForm({ ...form, username })} />
                    </View>
                </View>
                <View style={styles.row}>
                    <Text style={styles.label}>Email</Text>
                    <View>
                        <TextInput defaultValue={form.email}
                            style={styles.input} onChangeText={email => setForm({ ...form, email })} />
                    </View>
                </View>
            </ScrollView>
            <View style={styles.button}>
                <TouchableOpacity onPress={saveAccount} style={[styles.button__click, { backgroundColor: color.primary }]}>
                    <Text style={{ color: color.light, textAlign: 'center', fontWeight: 'bold' }}>Update Account</Text>
                </TouchableOpacity>
            </View>

            <Snackbar visible={snack.visibility === 'open'} onDismiss={() => setSnack({ ...snack, visibility: 'closed' })}>
                {snack.body}
            </Snackbar>
            <LoadingDialog dismissable={false} progress={progress} />

            <Dialog dismissable visible={cameraDialog === 'open'} onDismiss={() => setCameraDialog('closed')}>
                <Dialog.Content>
                    <TouchableOpacity style={styles.dialog__button} onPress={triggerImagePicker.bind(null, 'Camera')}>
                        <Text>Camera</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.dialog__button} onPress={triggerImagePicker.bind(null, 'Gallery')}>
                        <Text>Gallery</Text>
                    </TouchableOpacity>
                </Dialog.Content>
            </Dialog>
        </View>
    )
}

export default Account;
