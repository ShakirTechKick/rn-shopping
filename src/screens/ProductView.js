import React, { Component } from 'react';
import { View, Text, StyleSheet, Platform, ImageBackground, Image, useWindowDimensions } from 'react-native';
import AppHeader from '../components/AppHeader';
import { Left, Icon, Body, Title } from 'native-base';
import { TouchableOpacity, ScrollView, FlatList } from 'react-native-gesture-handler';
import Swiper from 'react-native-swiper';
import { useStoreState, useStoreActions } from 'easy-peasy';
import Product from '../components/Product';
import useThemeColor from '../helpers/useThemeColor';
import HTMLView from 'react-native-htmlview';

import FastGallery from 'react-native-fast-image';

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    banner: {
        width: '100%',
        height: 250, zIndex: -1,
        marginTop: -80
    },
    section: {
        flex: 1,
        marginVertical: 10
    },
    header: {
        width: '100%',
        padding: 15,
        marginBottom: 5
    },
    content: {
        padding: 15
    },
    bottom: {
        width: '100%',
        flexDirection: 'row'
    },
    bottom_button: {
        padding: 15,
        backgroundColor: 'green'
    }
})

const ProductView = (props) => {

    const { color } = useThemeColor();
    const { width, height } = useWindowDimensions();

    const { product: item } = props.route.params;
    const categories = useStoreState(state => state.categories.list);
    const products = useStoreState(state => state.products.list);

    const cart = useStoreState(state => state.cart.list);
    const addToCart = useStoreActions(actions => actions.cart.addToCart);


    const renderProduct = ({ item, index }) => {

        return (
            <Product navigation={props.navigation} key={item.product_id}
                product={item} />
        )
    }

    const onContinueShopping = () => {
        props.navigation.pop();
    }

    const onAddToCart = () => {
        addToCart(item)
    }

    const onViewCart = () => {
        props.navigation.navigate('CartModal');
    }

    return (
        <View style={[styles.container, { backgroundColor: color.background }]}>
            <AppHeader transparent  {...props}
                statusBarColor='transparent'
                headerBackgroundColor={'rgba(50, 50, 50, 0.5)'}
                extraStyles={Platform.OS === 'android' ? { elevation: 0 } : { shadowColor: 'transparent' }}
                renderLeftComponent={() => (
                    <Left>
                        <TouchableOpacity onPress={() => props.navigation.pop()}>
                            <Icon name='keyboard-arrow-left' style={{ color: color.light }} type='MaterialIcons' />
                        </TouchableOpacity>
                    </Left>
                )}
                renderBodyComponent={() => (
                    <Body><Title style={{ color: color.light }}>{item.product}</Title></Body>
                )}
            />

            <View style={styles.banner}>
                <Swiper horizontal={false} autoplay={true} activeDotColor={color.shades}>
                    {item.images && item.images.map((banner, key) => (
                        <FastGallery key={key} style={{ width: '100%', height: 250 }}
                            resizeMode='cover' source={{ uri: banner }} />
                    ))
                    }
                </Swiper>
            </View>
            <ScrollView
                style={[styles.container]}>

                <View style={[styles.section]}>
                    <View style={[styles.header, { backgroundColor: color.background }]}>
                        <Text style={{ fontWeight: 'bold' }}>Product Description</Text>
                    </View>
                    <View style={[styles.content, { backgroundColor: color.background }]}>
                        <HTMLView value={item.description}/>
                    </View>
                </View>

                {item.color && <View style={[styles.section]}>
                    <View style={[styles.header, { backgroundColor: color.background }]}>
                        <Text style={{ fontWeight: 'bold' }}>Available Colors</Text>
                    </View>
                    <View style={[styles.content, { backgroundColor: color.background, flexDirection: 'row' }]}>
                        {
                            Array.isArray(JSON.parse(item.color)) && [...JSON.parse(item.color)].map((c, index) => (
                                <View key={index} style={{
                                    width: 25, height: 25, backgroundColor: c,
                                    margin: 10, borderColor: color.light, borderWidth: 2
                                }} />
                            ))
                        }
                    </View>
                </View>}

                <View style={[styles.section]}>

                    <View style={[styles.header, { backgroundColor: color.background }]}>
                        <Text style={{ fontWeight: 'bold' }}>Similar Products</Text>
                    </View>

                    <View style={[styles.content, { backgroundColor: color.background }]}>
                        <FlatList
                            horizontal renderItem={renderProduct}
                            data={products.sort((a, b) => b.product_id - a.product_id).filter(p =>
                                (p.category_id === item.category_id &&
                                    p.product_id !== item.product_id
                                )
                            )} keyExtractor={(item, index) => item.product_id} />
                    </View>

                </View>

            </ScrollView>
            <View style={styles.bottom}>
                {item.stock > 0 ?
                    <React.Fragment>
                        {cart.find(c => c.product_id === item.product_id) ?
                            <TouchableOpacity onPress={onViewCart} style={[styles.bottom_button, { width: width / 2, backgroundColor: color.dark }]}>
                                <Text style={{ color: color.light, textAlign: 'center', fontWeight: 'bold' }}>VIEW CART</Text>
                            </TouchableOpacity> :
                            <TouchableOpacity onPress={onAddToCart} style={[styles.bottom_button, { width: width / 2, backgroundColor: color.primary }]}>
                                <Text style={{ color: color.light, textAlign: 'center', fontWeight: 'bold' }}>ADD TO CART</Text>
                            </TouchableOpacity>
                        }
                    </React.Fragment> :
                    <View style={[styles.bottom_button, { width: width / 2, backgroundColor: color.light }]}>
                        <Text style={{ color: 'red', textAlign: 'center', fontWeight: 'bold' }}>OUT OF STOCK</Text>
                    </View>
                }
                <TouchableOpacity style={[styles.bottom_button, { width: width / 2 }]} onPress={onContinueShopping}>
                    <Text style={{ color: color.light, textAlign: 'center', fontWeight: 'bold' }}>CONTINUE SHOPPING</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

export default ProductView;