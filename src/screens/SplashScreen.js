import React, { Component } from 'react';
import { StyleSheet, View, Text, StatusBar, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import Strings from '../constants/Strings';
import DataProvider from '../provider/DataProvider';
import { useStoreActions, useStoreRehydrated, useStoreState } from 'easy-peasy';
import useThemeColor from '../helpers/useThemeColor';
import { FetchDataFromServer } from '../services/BackgroundDataFetch';
import RNBootSplash from "react-native-bootsplash";

const Splash = ({ navigation }) => {

    const { color } = useThemeColor();

    const isStoreHydrated = useStoreRehydrated();

    React.useEffect(() => {

        FetchDataFromServer();
        
        RNBootSplash.hide({ duration: 250 });

        if (isStoreHydrated) {
            setTimeout(() => {
                navigation.navigate('AppScreen');
            }, 300)

        }

    }, [isStoreHydrated])



    return (
        <>
            <StatusBar animated backgroundColor={color.primary} />
            <View style={[styles.root, { backgroundColor: color.primary }]}>
                <ActivityIndicator shouldRasterizeIOS size='large' color={color.light} />
            </View>
        </>
    );
}



const styles = StyleSheet.create({
    root: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    label: {
        fontWeight: 'bold',
        fontSize: 25,
        marginBottom: 25
    }
})

export default Splash;