import React from 'react'
import { View, Text, StyleSheet, TextInput, useWindowDimensions, StatusBar } from 'react-native';
import { ActivityIndicator, TextInput as TextInputPaper, Snackbar } from 'react-native-paper';
import CountryPicker from 'react-native-country-picker-modal';
import AnimatedView from '../components/AnimatedView';
import { Icon, Button, Body, Right, Left } from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AppHeader from '../components/AppHeader';
import OTPHandlerInput from "react-native-otp-inputs";
import LoadingDialog from '../components/LoadingDialog';
import { useStoreState, useStoreActions } from 'easy-peasy';
import useThemeColor from '../helpers/useThemeColor';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15,
        justifyContent: 'center', alignItems: 'center'
    },
    form: {
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 5,
        padding: 5
    },
    header: {
        fontSize: 25,
        marginVertical: 50
    },
    button: {
        marginVertical: 50,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        borderRadius: 50
    },
    back: { flexDirection: 'row', alignItems: 'center' }
})

const LoginScreen = ({ navigation, route }) => {

    const { color } = useThemeColor();

    const history = route.params.history;
    const payload = route.params.payload;


    const { user } = useStoreState(state => state.auth);

    const [snack, setSnack] = React.useState({ visibility: 'closed', body: null });
    const number = route.params.number || null;
    const [pin, setPin] = React.useState(route.params.otp || null);

    const { width, height } = useWindowDimensions();

    const [progress, setProgress] = React.useState(false);

    const onBackPress = () => {
        navigation.pop();
    }

    const onPressNext = () => {
        setProgress(!progress);

        setTimeout(() => {
            if (user && (pin === user.pin)) {

                setSnack({
                    visibility: 'open',
                    body: 'Successfully Logged', background: 'green'
                });

                setTimeout(() => {

                    setProgress(!progress);

                    if (user.status === 'new') {
                        navigation.navigate('RegisterView', { history, payload, number });
                    } else {
                        if (history) {
                            navigation.navigate(history, { ...payload });
                        } else {
                            navigation.navigate('AppScreen');
                        }
                    }

                }, 300)


            } else {
                setProgress(!progress);
                setSnack({ visibility: 'open', body: 'OTP verification failed !' });
            }
        }, 2000)

    }

    return (
        <>
            <AppHeader transparent headerBackgroundColor={color.dark} statusBarColor={'transparent'} renderLeftComponent={() => (
                <Left>
                    <TouchableOpacity style={styles.back} onPress={onBackPress}>
                        <Icon name='keyboard-arrow-left' style={{ color: color.light }} type='MaterialIcons' />
                        <Text style={{ color: color.light, marginBottom: 2 }}>Back</Text>
                    </TouchableOpacity>
                </Left>
            )} renderBodyComponent={() => <Body />} renderRightComponent={() => <Right />} />
            <View style={[styles.container, { backgroundColor: color.dark }]}>
                <View style={styles.header}>
                    <Text style={{ color: color.light, fontSize: 25, fontWeight: 'bold' }}>Verify OTP</Text>
                    <Text style={{ color: 'rgba(250, 250, 250, 0.5)' }}>We have sent an OTP to {number} via an SMS.</Text>
                </View>
                <View style={[styles.form, { backgroundColor: color.light }]}>
                    <OTPHandlerInput numberOfInputs={6}
                        handleChange={(pin) => setPin(pin)}
                        inputStyles={{ borderBottomColor: 'transparent' }}
                        caretHidden defaultValue={pin} />
                </View>
                <AnimatedView styles={[styles.button, { backgroundColor: color.light }]} action={onPressNext}>
                    <Icon name='keyboard-arrow-right' type='MaterialIcons' />
                </AnimatedView>
            </View>

            <Snackbar style={{ backgroundColor: snack.background ? snack.background : color.background }} visible={snack.visibility === 'open'} onDismiss={() => setSnack({ ...snack, visibility: 'closed' })}>
                {snack.body}
            </Snackbar>

            <LoadingDialog dismissable={false} progress={progress} />
        </>
    )

}

export default LoginScreen;
