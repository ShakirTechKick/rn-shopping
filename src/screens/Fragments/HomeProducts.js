import React from 'react';
import { useStoreState } from 'easy-peasy';
import RecyclerView from '../../components/RecyclerView';
import { ImageBackground, ScrollView, StyleSheet, View, Text } from 'react-native';
import Banners from '../../components/Banners';
import HeaderBar from '../../components/HeaderBar';
import { TouchableOpacity } from 'react-native-gesture-handler';
import useThemeColor from '../../helpers/useThemeColor';
import { ActivityIndicator } from 'react-native-paper';

const styles = StyleSheet.create({
    container: { flex: 1 },
    section: {
        flex: 1,
    },
    item: {
        width: 100, height: 100, alignItems: 'center',
        margin: 10, borderRadius: 10, elevation: 5,
        shadowOffset: { width: 5, height: 10 }, justifyContent: 'center'
    }
})

const HomeProducts = (props) => {

    const { color } = useThemeColor();

    const categories = useStoreState(state => state.categories.list);
    const brands = useStoreState(state => state.brands.list);
    const products = useStoreState(state => state.products.list);

    const navigateToShopView = (item, heading) => {
        props.navigation.navigate('ShopView', { item, heading });
    }

    const renderCategories = ({ item, index }) => {
        return (
            <TouchableOpacity key={item.category_id} onPress={navigateToShopView.bind(null, item, "Categories")}
                style={[styles.item, { backgroundColor: color.light }]}>

                <ImageBackground source={{ uri: item.image }}
                    resizeMode='contain'
                    style={{ width: 50, height: 50, borderRadius: 10, alignSelf: 'center' }} />
                <View>
                    <Text style={{ fontSize: 10, textAlign: 'center', fontWeight: 'bold' }}>{item.category}</Text>
                    <Text style={{ fontSize: 10, textAlign: 'center' }}>
                        ({products && products.filter(p => p.category_id === item.category_id).length} Products)
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }

    const renderBrands = ({ item, index }) => {
        return (
            <TouchableOpacity key={item.brand_id} onPress={navigateToShopView.bind(null, item, "Brands")} style={[styles.item, { backgroundColor: color.light }]}>

                <ImageBackground source={{ uri: item.image }}
                    resizeMode='cover'
                    style={{ width: 50, height: 50, borderRadius: 5 }} />
                <View>
                    <Text style={{ fontSize: 10, textAlign: 'center', fontWeight: 'bold' }}>{item.brand}</Text>
                    <Text style={{ fontSize: 10, textAlign: 'center' }}>
                        ({products && products.filter(p => p.brand_id === item.brand_id).length} Products)
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }



    return (
        <ScrollView style={styles.container}>

            <Banners />

            <View style={styles.section}>
                <HeaderBar text="Shop By Categories" />
                <View style={{ flexWrap: 'wrap', flexDirection: 'row', justifyContent: 'center' }}>
                    {
                        categories.map((category, index) => {
                            return (
                                <React.Fragment key={category.category_id}>
                                    {renderCategories({ item: category, index })}
                                </React.Fragment>
                            )
                        })
                    }
                </View>
            </View>

            <View style={styles.section}>
                <HeaderBar text="Shop By Brands" />
                <View style={{ flexWrap: 'wrap', flexDirection: 'row', justifyContent: 'center' }}>
                    {
                        brands.map((brand, index) => {
                            if (index < 6) {
                                return (
                                    <React.Fragment key={brand.brand_id}>
                                        {renderBrands({ item: brand, index })}
                                    </React.Fragment>
                                )
                            }
                        })
                    }
                </View>
            </View>

            <HeaderBar text="Latest Products" />
            {   (products && products.length) ?
                <RecyclerView products={products} full navigation={props.navigation} banners /> :

                <View style={{
                    flex: 1, padding: 15, flexDirection: 'row',
                    justifyContent: 'center', alignItems: 'center'
                }}>
                    <ActivityIndicator color={color.primary} size={15} />
                    <Text style={{fontWeight: 'bold', paddingHorizontal: 10}}>Fetching Products ...</Text>
                </View>
            }
        </ScrollView>
    )
}

export default HomeProducts;