import React from 'react';
import { View, StyleSheet } from 'react-native';
import { useStoreState } from 'easy-peasy';
import HeaderBar from '../../components/HeaderBar';
import RecyclerView from '../../components/RecyclerView';

const styles = StyleSheet.create({
    container: { flex: 1 }
})


const TopSeller = (props) => {

    const sellers = useStoreState(state => state.sellers.list);

    return (
        <View style={styles.container}>
            <HeaderBar text="Top Sellers" />
            {   (sellers && sellers.length) &&
                <RecyclerView full products={sellers} kind='seller' />
            }
        </View>
    )
}

export default TopSeller;