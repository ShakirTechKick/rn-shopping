import React from 'react';
import { View, StyleSheet } from 'react-native';
import { useStoreState } from 'easy-peasy';
import HeaderBar from '../../components/HeaderBar';
import RecyclerView from '../../components/RecyclerView';


const styles = StyleSheet.create({
    container: { flex: 1 }
})

const Deals = (props) => {

    const deals = useStoreState(state => state.deals.list);


    return (
        <View style={styles.container}>
            <HeaderBar text="Flash Deals" />
            {(deals && deals.length) &&
                <RecyclerView full products={deals} kind='deals' />
            }
        </View>
    )
}

export default Deals;