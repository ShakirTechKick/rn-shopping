import React from 'react';
import { View, StyleSheet } from 'react-native';
import { useStoreState } from 'easy-peasy';
import HeaderBar from '../../components/HeaderBar';
import RecyclerView from '../../components/RecyclerView';

const styles = StyleSheet.create({
    container: { flex: 1 }
})


const MostLiked = (props) => {

    const wanted = useStoreState(state => state.wanted.list);

    return (
        <View style={styles.container}>
            <HeaderBar text="Most Liked" />
            { (wanted && wanted.length) &&
                <RecyclerView full products={wanted} kind='wanted' />
            }
        </View>
    )
}

export default MostLiked;