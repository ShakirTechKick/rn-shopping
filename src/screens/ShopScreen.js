import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, BackHandler, SafeAreaView, StyleSheet, ImageBackground } from 'react-native';
import AppHeader from '../components/AppHeader';
import { Body, Title, Left, Right, Icon, Button } from 'native-base';
import { FlatList, TouchableOpacity, ScrollView, TextInput } from 'react-native-gesture-handler';
import { useStoreState } from 'easy-peasy';
import useThemeColor from '../helpers/useThemeColor';
import ScrolledTabView from '../components/ScrolledTabView';
import Product from '../components/Product';
import { Portal, Dialog, List, RadioButton } from 'react-native-paper';
import moment from 'moment';

const styles = StyleSheet.create({
    container: { flex: 1 },
    content: { flex: 1 },
    category: {
        flex: 1,
        marginVertical: 10,
        height: 75,
        borderRadius: 5,
        flexDirection: 'row', padding: 5,
        alignItems: 'center', justifyContent: 'flex-start'
    },
    header: {
        fontWeight: 'bold',
        fontSize: 15, marginBottom: 5
    },
    products: {
        fontSize: 10,
        color: 'grey'
    },
    row: {
        marginVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    action: {
        flex: 1, flexDirection: 'row',
        alignItems: 'center', justifyContent: 'space-between',
        padding: 10
    },
    button: {
        padding: 5,
        justifyContent: 'center'
    },
    applied: {
        width: 10, height: 10, borderRadius: 50, margin: 5,
        position: 'absolute', backgroundColor: 'rgba(250, 250, 250, 0.95)',
        justifyContent: 'center', alignItems: 'center'
    },
    dot: { backgroundColor: 'red', width: 5, height: 5 }
})

const Shop = (props) => {

    const filter__object = {
        visibility: 'closed', isApplied: false, sort: 'Newest', price: { min: null, max: null }
    }
    const [filter, setFilter] = React.useState(filter__object);
    const [expanded, setExpanded] = React.useState({
        pricing: false, sort: true
    });

    const applyFilter = () => {
        setFilter({ ...filter, isApplied: true, visibility: 'closed' });
    }

    const clearFiler = () => {
        setFilter({ ...filter__object });
    }


    const categories = useStoreState(state => state.categories.list);
    const brands = useStoreState(state => state.brands.list);
    const products = useStoreState(state => state.products.list);

    var current = 0;
    var heading = "Categories";

    if (props.route.params && props.route.params.item) {
        const item = props.route.params.item;
        heading = props.route.params.heading;

        if (heading === 'Categories') {
            current = categories.findIndex(c => c.category_id === item.category_id) + 1;
        }

        if (heading === 'Brands') {
            current = brands.findIndex(b => b.brand_id === item.brand_id) + 1;
        }
    }

    const { color } = useThemeColor();

    const renderShopProductsContent = (data) => {

        const renderProduct = ({ item, index }) => {
            return (
                <Product navigation={props.navigation} key={item.product_id}
                    product={item} />
            )
        }

        return (
            <ScrollView style={{ flex: 1 }}>
                <View style={{ flexDirection: 'row', marginVertical: 15, flexWrap: 'wrap', justifyContent: 'flex-start' }}>
                    {
                        data.map((product, key) => (
                            renderProduct({ item: product, index: key })
                        ))
                    }
                </View>
            </ScrollView>
        )
    }

    const onPressFilterTrigger = () => {
        setFilter({ ...filter, visibility: 'open' });
    }

    const onDismissFilter = () => {
        setFilter({ ...filter, visibility: 'closed' });
    }


    const filterBasedProducts = (items = []) => {
        var filtered__products = [];

        filtered__products = items.sort((a, b) => moment(a.createdAt).diff(b.createdAt));

        if (filter.isApplied) {
            const sort = filter.sort;

            switch (sort) {
                case 'Newest':
                    filtered__products = items.sort((a, b) => moment(a.createdAt).diff(b.createdAt));
                    break;
                case 'Oldest':
                    filtered__products = items.sort((a, b) => moment(b.createdAt).diff(a.createdAt));
                    break;
                case 'A - Z':
                    filtered__products = items.sort((a, b) => a.product > b.product);
                    break;
                case 'Z - A':
                    filtered__products = items.sort((a, b) => b.product > a.product);
                    break;
                case 'Price: High - Low':
                    filtered__products = items.sort((a, b) => b.price - a.price);
                    break;
                case 'Price: Low - High':
                    filtered__products = items.sort((a, b) => a.price - b.price);
                    break;
                default:
                    filtered__products = items.sort((a, b) => moment(a.createdAt).diff(b.createdAt));
            }

            if (filter.price.min) {
                filtered__products = filtered__products.filter(p => p.price > filter.price.min);
            }

            if (filter.price.max) {
                filtered__products = filtered__products.filter(p => p.price < filter.price.max);
            }

            return filtered__products;
        }
        return filtered__products;
    }

    return (
        <View style={styles.container}>
            <AppHeader {...props}
                renderBodyComponent={() => (<Body><Title>By {heading}</Title></Body>)}
                renderLeftComponent={() => (
                    <Left>
                        <TouchableOpacity onPress={() => props.navigation.pop()}>
                            <Icon name='keyboard-arrow-left' style={{ color: color.light }} type='MaterialIcons' />
                        </TouchableOpacity>
                    </Left>
                )}
                renderRightComponent={() => (
                    <Right>
                        <TouchableOpacity onPress={onPressFilterTrigger}>
                            {filter.isApplied &&
                                <View style={styles.applied}>
                                    <View style={styles.dot}></View>
                                </View>
                            }
                            <Button transparent><Icon name='filter-list' type='MaterialIcons' /></Button>
                        </TouchableOpacity>
                    </Right>
                )}
            />

            <ScrollView style={[styles.content]}>

                {heading === 'Categories' &&
                
                    <ScrolledTabView
                        current={current}
                        tabs={categories && ["All", ...categories.sort((a, b) => a.category_id - a.category_id).map(c => c.category)]}
                        data={[
                            filterBasedProducts(products),
                            ...categories.map(c => {
                                return filterBasedProducts(products).filter(p => p.category_id === c.category_id)
                            })
                        ]}
                        renderContent={renderShopProductsContent}
                    />

                }

                {heading === 'Brands' &&
                    <ScrolledTabView
                        current={current}
                        tabs={brands && ["All", ...brands.sort((a, b) => a.brand_id - a.brand_id).map(b => b.brand)]}
                        data={[
                            products,
                            ...brands.map(b => {
                                return products.filter(p => p.brand_id === b.brand_id)
                            })
                        ]}
                        renderContent={renderShopProductsContent}
                    />

                }
            </ScrollView>

            <Portal>
                <Dialog visible={filter.visibility === 'open'} onDismiss={onDismissFilter}>
                    <Dialog.Content>
                        <List.Section>
                            <List.Accordion
                                title="Sort" expanded={expanded.sort} style={{ backgroundColor: color.background }}
                                onPress={() => setExpanded({ ...expanded, pricing: false, sort: !expanded.sort })}>
                                <RadioButton.Group
                                    onValueChange={sort => setFilter({ ...filter, sort })} value={filter.sort}>
                                    <View style={[styles.row, { justifyContent: 'flex-start' }]}>
                                        <RadioButton value="Newest" theme={{ colors: { accent: color.primary } }} />
                                        <Text>Newest</Text>
                                    </View>
                                    <View style={[styles.row, { justifyContent: 'flex-start' }]}>
                                        <RadioButton value="A - Z" theme={{ colors: { accent: color.primary } }} />
                                        <Text>A - Z</Text>
                                    </View>
                                    <View style={[styles.row, { justifyContent: 'flex-start' }]}>
                                        <RadioButton value="Z - A" theme={{ colors: { accent: color.primary } }} />
                                        <Text>Z - A</Text>
                                    </View>
                                    <View style={[styles.row, { justifyContent: 'flex-start' }]}>
                                        <RadioButton value="Price: High - Low" theme={{ colors: { accent: color.primary } }} />
                                        <Text>Price: High - Low</Text>
                                    </View>
                                    <View style={[styles.row, { justifyContent: 'flex-start' }]}>
                                        <RadioButton value="Price: Low - High" theme={{ colors: { accent: color.primary } }} />
                                        <Text>Price: Low - High</Text>
                                    </View>
                                </RadioButton.Group>
                            </List.Accordion>
                            <List.Accordion
                                title="Pricing" expanded={expanded.pricing} style={{ backgroundColor: color.background }}
                                onPress={() => setExpanded({ ...expanded, sort: false, pricing: !expanded.pricing })}>
                                <View style={[styles.row, { justifyContent: 'flex-start' }]}>
                                    <Text>Min</Text>
                                    <TextInput value={filter.price.min} onChangeText={min => setFilter({ ...filter, price: { ...filter.price, min } })}
                                        placeholder='Min Amount' style={{ width: 200, borderBottomColor: 'transparent', padding: 10 }}
                                        underlineColorAndroid='transparent' keyboardType='decimal-pad' />
                                </View>
                                <View style={[styles.row, { justifyContent: 'flex-start' }]}>
                                    <Text>Max</Text>
                                    <TextInput value={filter.price.max} onChangeText={max => setFilter({ ...filter, price: { ...filter.price, max } })}
                                        placeholder='Max Amount' style={{ width: 200, borderBottomColor: 'transparent', padding: 10 }}
                                        underlineColorAndroid='transparent' keyboardType='decimal-pad' />
                                </View>
                            </List.Accordion>
                        </List.Section>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <View style={styles.action}>
                            <Button transparent style={[styles.button]} onPress={clearFiler}>
                                <Text style={{ color: 'red', fontWeight: 'bold' }}>Clear</Text>
                            </Button>
                            <Button transparent style={[styles.button]} onPress={applyFilter}>
                                <Text style={{ color: 'green', fontWeight: 'bold' }}>Apply</Text>
                            </Button>
                        </View>
                    </Dialog.Actions>
                </Dialog>
            </Portal>

        </View>
    );
}

Shop.propTypes = {

};

export default Shop;