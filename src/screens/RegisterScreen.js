import React from 'react'
import { View, Text, StyleSheet, TextInput, useWindowDimensions, StatusBar, KeyboardAvoidingView } from 'react-native';
import { ActivityIndicator, TextInput as TextInputPaper, Snackbar } from 'react-native-paper';
import CountryPicker from 'react-native-country-picker-modal';
import AnimatedView from '../components/AnimatedView';
import { Icon, Button, Body, Right, Left } from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AppHeader from '../components/AppHeader';
import { useStoreActions, useStoreState } from 'easy-peasy';
import LoadingDialog from '../components/LoadingDialog';
import useThemeColor from '../helpers/useThemeColor';
import { Request } from '../services/Request';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15,
        justifyContent: 'center', alignItems: 'center'
    },
    form: {
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 5,
        padding: 5,
        marginVertical: 10
    },
    header: {
        fontSize: 25,
        marginTop: 75, marginBottom: 25
    },
    button: {
        flexDirection: 'row',
        marginVertical: 25,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5,
        borderRadius: 50
    },
    back: { flexDirection: 'row', alignItems: 'center' }
})

const RegisterScreen = ({ route, navigation }) => {


    const history = route.params.history;
    const payload = route.params.payload;

    const { color } = useThemeColor();

    const [snack, setSnack] = React.useState({ visibility: 'closed', body: null });

    const user = useStoreState(state => state.auth.user);
    const setUser = useStoreActions(actions => actions.auth.setUser);

    const [progress, setProgress] = React.useState(false);

    const [details, setDetails] = React.useState({ name: '', email: '', password: '', repassword: '' })

    const onBackPress = () => {
        navigation.pop();
    }

    const isPasswordvalid = () => {
        return (
            (details.password.length && details.repassword.length)
            && (details.password === details.repassword)
        )
    }

    const onPressNext = () => {

        if (details.name.length && details.email.length) {

            if (isPasswordvalid()) {

                setProgress(!progress);

                Request.post('registration', { user_id: user.user_id, ...details })
                    .then(response => {
                        if (response.status === 200) {
                            setUser({ ...response.data.user });
                            setProgress(!progress);
                            setSnack({ visibility: 'open', body: 'Successfully Registered', background: 'green' });

                            setTimeout(() => {
                                if (history) {
                                    navigation.navigate(history, { ...payload });
                                } else {
                                    navigation.navigate('AppScreen');
                                }
                            }, 300)
                        } else {
                            setProgress(!progress);
                            setSnack({ visibility: 'open', body: 'Server error !' });
                        }
                    }).catch(error => {
                        setProgress(!progress);
                        setSnack({ visibility: 'open', body: 'Name and Email are required !' });
                    })
            } else {
                setSnack({ visibility: 'open', body: 'Password is required and match !' });
            }

        } else {
            setSnack({ visibility: 'open', body: 'Name and Email are required !' });
        }

    }

    return (
        <>
            <AppHeader transparent headerBackgroundColor={color.dark} statusBarColor={'transparent'} renderLeftComponent={() => (
                <Left>
                    <TouchableOpacity style={styles.back} onPress={onBackPress}>
                        <Icon name='keyboard-arrow-left' style={{ color: color.light }} type='MaterialIcons' />
                        <Text style={{ color: color.light, marginBottom: 2 }}>Back</Text>
                    </TouchableOpacity>
                </Left>
            )} renderBodyComponent={() => <Body />} renderRightComponent={() => <Right />} />
            <View style={[styles.container, { backgroundColor: color.dark }]}>
                <KeyboardAvoidingView behavior='padding'>
                    <View style={styles.header}>
                        <Text style={{ color: color.light, fontSize: 25, fontWeight: 'bold' }}>Enter Your Information</Text>
                    </View>
                    <View style={[styles.form, { backgroundColor: color.light }]}>
                        <TextInput underlineColorAndroid='transparent'
                            placeholder='Full Name' selectionColor={color.dark} keyboardType='default' onChangeText={(text) => setDetails({ ...details, name: text })}
                            style={{
                                width: '100%', padding: 10,
                                borderBottomColor: 'transparent', marginHorizontal: 5
                            }} />
                    </View>
                    <View style={[styles.form, { backgroundColor: color.light }]}>
                        <TextInput underlineColorAndroid='transparent'
                            placeholder='Email Address' selectionColor={color.dark} keyboardType='email-address' onChangeText={(text) => setDetails({ ...details, email: text })}
                            style={{
                                width: '100%',
                                borderBottomColor: 'transparent',
                                padding: 10, marginHorizontal: 5
                            }} />
                    </View>

                    <View style={[styles.form, { backgroundColor: color.light }]}>
                        <TextInput underlineColorAndroid='transparent'
                            placeholder='Password' selectionColor={color.dark} keyboardType='default' onChangeText={(text) => setDetails({ ...details, password: text })}
                            style={{
                                width: '100%',
                                borderBottomColor: 'transparent',
                                padding: 10, marginHorizontal: 5
                            }} secureTextEntry />
                    </View>

                    <View style={[styles.form, { backgroundColor: color.light }]}>
                        <TextInput underlineColorAndroid='transparent'
                            placeholder='Re Password' selectionColor={color.dark} keyboardType='default' onChangeText={(text) => setDetails({ ...details, repassword: text })}
                            style={{
                                width: '100%',
                                borderBottomColor: 'transparent',
                                padding: 10, marginHorizontal: 5
                            }} secureTextEntry />
                    </View>

                </KeyboardAvoidingView>
                <AnimatedView max={1.25} styles={[styles.button, { backgroundColor: color.light, width: 250 }]} action={onPressNext}>
                    <Text>Register</Text>
                    <Icon name='keyboard-arrow-right' type='MaterialIcons' />
                </AnimatedView>
            </View>


            <Snackbar style={{ backgroundColor: snack.background ? snack.background : color.background }} visible={snack.visibility === 'open'} onDismiss={() => setSnack({ ...snack, visibility: 'closed' })}>
                {snack.body}
            </Snackbar>

            <LoadingDialog dismissable={false} progress={progress} />
        </>
    )

}

export default RegisterScreen;
