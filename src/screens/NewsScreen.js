import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, BackHandler, SafeAreaView, StyleSheet } from 'react-native';
import AppHeader from '../components/AppHeader';
import { Body, Title } from 'native-base';

const styles = StyleSheet.create({
    content: {
        padding: 15
    }
})

const News = (props) => {

    return (
        <SafeAreaView>
           <AppHeader  {...props}  renderBodyComponent={() => (
                <Body><Title>News</Title></Body>
            )} />

            <View style={[styles.content]}>
                <Text>News Screen</Text>
            </View>
        </SafeAreaView>
    );
}

News.propTypes = {

};

export default News;