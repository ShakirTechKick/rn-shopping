import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, BackHandler, SafeAreaView, StyleSheet, ImageBackground, Dimensions, Image, useWindowDimensions, Easing, RefreshControl, PanResponder, Animated } from 'react-native';
import AppHeader from '../components/AppHeader';
import { useStoreState } from 'easy-peasy';
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import TopSeller from './Fragments/TopSeller';
import Deals from './Fragments/Deals';
import MostLiked from './Fragments/MostLiked';
import { ScrollView, FlatList, TouchableOpacity, TouchableNativeFeedback } from 'react-native-gesture-handler';
import ScrolledTabView from '../components/ScrolledTabView';
import Product from '../components/Product';
import useThemeColor from '../helpers/useThemeColor';
import { ActivityIndicator } from 'react-native-paper';
import RecyclerView from '../components/RecyclerView';
import { Icon } from 'native-base';
import HomeProducts from './Fragments/HomeProducts';


const styles = StyleSheet.create({
    parent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        flex: 1
    },
    content: {
        flex: 1,
        padding: 0
    }
})


const PrimaryHomeTabs = props => {

    const { color } = useThemeColor();

    const { width, height } = useWindowDimensions();
    const PrimaryBottomTabContainer = createMaterialBottomTabNavigator();

    return (
        <PrimaryBottomTabContainer.Navigator sceneAnimationEnabled
            activeColor={color.primary}
            initialLayout={{ width, height }} barStyle={{ backgroundColor: color.light }}>
            <PrimaryBottomTabContainer.Screen name='Products'
                options={{
                    tabBarLabel: 'Products', tabBarIcon: ({ focused, color: c }) => {
                        return <Icon name='home' style={{ color: focused ? color.primary : c, fontSize: 25 }} type='MaterialIcons' />
                    }
                }} component={HomeProducts} />


            <PrimaryBottomTabContainer.Screen name='Deals'
                options={{
                    tabBarLabel: 'Deals', tabBarIcon: ({ focused, color: c }) => {
                        return <Icon name='flash-on' style={{ color: focused ? color.primary : c, fontSize: 25 }} type='MaterialIcons' />
                    }
                }} component={Deals} />
            <PrimaryBottomTabContainer.Screen name='TopSeller'
                options={{
                    tabBarLabel: 'Top Sellers', tabBarIcon: ({ focused, color: c }) => {
                        return <Icon name='arrow-up' style={{ color: focused ? color.primary : c, fontSize: 25 }} type='EvilIcons' />
                    }
                }} component={TopSeller} />
            <PrimaryBottomTabContainer.Screen name='MostLiked'
                options={{
                    tabBarLabel: 'Most Liked', tabBarIcon: ({ focused, color: c }) => {
                        return <Icon name='heart' style={{ color: focused ? color.primary : c, fontSize: 25 }} type='EvilIcons' />
                    }
                }} component={MostLiked} />
        </PrimaryBottomTabContainer.Navigator>
    )

}


const HomeScreen = (props) => {

    return (
        <View style={styles.container}>

            <AppHeader {...props}/>

            <PrimaryHomeTabs />

        </View>
    );
}


HomeScreen.propTypes = {

};

export default HomeScreen;