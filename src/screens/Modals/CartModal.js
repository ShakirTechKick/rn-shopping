import React, { Component } from 'react';
import { View, Text, BackHandler, StyleSheet, ImageBackground, Alert, useWindowDimensions, Image } from 'react-native';
import { Right, Left, Body, Title, Icon, Button, Grid, Col, Row } from 'native-base';
import AppHeader from '../../components/AppHeader';
import { TouchableOpacity, ScrollView, TextInput } from 'react-native-gesture-handler';
import { useStoreState, useStoreActions } from 'easy-peasy';
import useThemeColor from '../../helpers/useThemeColor';
import { RadioButton } from 'react-native-paper';
import FastGallery from 'react-native-fast-image';
import SelectionPicker from '../../components/components/SelectionPicker';
import ColorPicker from '../../components/components/ColorPicker';
const CartIconEmpty = require('../../assets/icons/cart.png');

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    block: {
        margin: 10
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomWidth: 0.5,
        padding: 10
    },
    body: {
        padding: 10
    },
    footer: {
        borderTopWidth: 0.5,
        padding: 10,
        flexDirection: 'row', justifyContent: 'flex-end'
    },
    quantity: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
        borderWidth: 0.5
    },
    button: { marginHorizontal: 5 },
    bottom: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        padding: 15
    },
    row: {
        marginEnd: 5
    }
});

const QuantityContainer = ({ product, quantity, updateCartProduct, removeFromCart }) => {

    const { color } = useThemeColor();

    const add = () => {
        updateCartProduct({ ...product, quantity: product.quantity + 1 });
    }

    const substract = () => {
        if (product.quantity - 1 > 0) {
            updateCartProduct({ ...product, quantity: product.quantity - 1 })
        } else {
            Alert.alert("Remove Product", "Are you sure to remove this item ?", [
                { text: 'No', style: 'cancel', onPress: () => null },
                {
                    text: 'Yes', style: 'default', onPress: () => {
                        removeFromCart(product)
                    }
                }
            ])
        }
    }

    return (
        <View style={[styles.quantity, { borderColor: color.dark }]}>
            <TouchableOpacity style={styles.button} onPress={substract}>
                <Icon name='minus' style={{ color: color.dark }} type='EvilIcons' />
            </TouchableOpacity>
            <View style={styles.button}>
                <Text style={{ textAlign: 'center' }}>{quantity}</Text>
            </View>
            <TouchableOpacity style={styles.button} onPress={add}>
                <Icon name='plus' style={{ color: color.dark }} type='EvilIcons' />
            </TouchableOpacity>
        </View>
    )
}

const Cart = (props) => {

    const { color } = useThemeColor();

    const user = useStoreState(state => state.auth.user);

    const cart = useStoreState(state => state.cart.list);
    const deals = useStoreState(state => state.deals.list);
    const categories = useStoreState(state => state.categories.list);
    const { removeFromCart, updateCartProduct } = useStoreActions(actions => actions.cart);

    const getTotalAmount = () => {
        var total = 0;

        cart.forEach((product) => {

            const price = deals.find(d => d.product_id === product.product_id) ?
                deals.find(d => d.product_id === product.product_id).offer : product.price;

            total += parseFloat(price) * parseInt(product.quantity);

            if (product.additional.length) {
                product.additional.forEach(add => {
                    total += parseFloat(add.amount);
                })
            }
        });

        return total.toFixed(2);
    }

    const checkoutShopping = () => {
        props.navigation.navigate('CheckOutView', { products: cart, total: getTotalAmount() })
    }
    const continueShopping = () => {
        props.navigation.navigate('HomeView');
    }

    const onRemoveProduct = (product) => {
        removeFromCart(product);
    }

    const redirectToLogin = () => {
        props.navigation.navigate('AuthScreen', { history: 'CheckOutView', payload: { products: cart, total: getTotalAmount() } });
    }

    const isColorSelection = (product) => {

        let color = false;

        if (product.color) {
            if (Array.isArray(JSON.parse(product.color))) {
                color = true;
            }
        }
        return color;
    }

    const onValueChangeColor = React.useCallback((current) => {

        const product = cart.find(c => c.product_id === current.split('-').shift());

        if (product) {
            const colors = product.colors || { selection: null };
            colors.selection = current;
            updateCartProduct({ ...product, colors });
        }
    }, [cart]);

    const onValueChangeSelection = React.useCallback((current) => {

        const product = cart.find(c => c.product_id === current.split('-').shift());

        if (product) {

            const selection = product.selections.find(s => s.name === current.split('-')[1]);

            if (selection) {

                const selections = product.selections;
                const additional = product.additional;

                const updates = { ...selection, current };

                if (additional.find(a => a.name === selection.name)) {
                    additional.splice(additional.findIndex(a => a.name === selection.name), 1,
                        {
                            name: selection.name, amount: selection.values
                                .find(criteria => criteria.value === current.split('-').pop()).additional
                        });
                } else {
                    additional.push({
                        name: selection.name, amount: selection.values
                            .find(criteria => criteria.value === current.split('-').pop()).additional
                    });
                }

                selections.splice(selections.findIndex(s => s.name === selection.name), 1, updates);
                updateCartProduct({ ...product, selections, additional });
            }
        }
    }, [cart])

    return (
        <View style={[styles.container, { backgroundColor: cart.length ? color.background : color.light }]}>
            <AppHeader  {...props}
                transparent={false} translucent={false} renderLeftComponent={() => (
                    <Left>
                        <TouchableOpacity onPress={() => props.navigation.pop()}>
                            <Icon name='keyboard-arrow-left' style={{ color: color.light }} type='MaterialIcons' />
                        </TouchableOpacity>
                    </Left>
                )}
                renderBodyComponent={() => (
                    <Body>
                        <Title style={{ color: color.light }}>My Shopping Cart</Title>
                    </Body>
                )}
                renderRightComponent={() => {
                    <Right></Right>
                }}
            />

            {cart.length > 0 ?
                <ScrollView style={[styles.container, { backgroundColor: color.background }]}>

                    {
                        cart.map((product, key) => {

                            return (
                                <View style={[styles.block, { backgroundColor: color.light }]}>
                                    <View style={[styles.header, { borderBottomColor: color.dark }]}>
                                        <FastGallery resizeMode='cover'
                                            style={{ width: 100, height: 50, marginEnd: 5 }}
                                            source={{ uri: product.images[0] }} />
                                        <View style={{ width: '50%' }}>
                                            <Text style={{ fontWeight: 'bold' }}>{product.product}</Text>
                                            <Text style={{ color: 'grey' }}>
                                                {categories.find(c => c.category_id === product.category_id) ?
                                                    categories.find(c => c.category_id === product.category_id).category : null}
                                            </Text>
                                        </View>
                                        <TouchableOpacity onPress={onRemoveProduct.bind(null, product)}>
                                            <Button transparent>
                                                <Icon name='cancel' style={{ color: 'red' }} type='MaterialIcons' />
                                            </Button>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.body}>
                                        <Grid>
                                            <Row style={{ marginVertical: 5 }}>
                                                <Col><Text style={{ fontWeight: 'bold' }}>Product Name</Text></Col>
                                                <Col><Text style={{ textAlign: 'right' }}>{product.product}</Text></Col>
                                            </Row>
                                            <Row style={{ marginVertical: 5 }}>
                                                <Col><Text style={{ fontWeight: 'bold' }}>Price</Text></Col>
                                                <Col>
                                                    <Text style={{ textAlign: 'right' }}>
                                                        {
                                                            (deals.find(d => d.product_id === product.product_id) ?
                                                                deals.find(d => d.product_id === product.product_id).offer : product.price)
                                                        }
                                                    </Text>
                                                </Col>
                                            </Row>
                                            <Row style={{ marginVertical: 5 }}>
                                                <Col><Text style={{ fontWeight: 'bold' }}>Quantity</Text></Col>
                                                <Col>
                                                    <QuantityContainer
                                                        product={product} quantity={product.quantity}
                                                        updateCartProduct={updateCartProduct} removeFromCart={removeFromCart} /></Col>
                                            </Row>
                                            {(isColorSelection(product)) &&
                                                <ColorPicker product={product} onValueChangeColor={onValueChangeColor}
                                                    current={product.colors ? product.colors.selection : null} color={color} />
                                            }

                                            {
                                                product.selections && product.selections.map((selection, index) => (
                                                    <SelectionPicker color={color} current={selection.current} onValueChangeSelection={onValueChangeSelection}
                                                        product={product} selection={selection} />
                                                ))
                                            }
                                        </Grid>
                                    </View>
                                </View>
                            )
                        })
                    }

                </ScrollView> :
                <View style={[styles.container, { justifyContent: 'center', alignItems: 'center' }]}>
                    <FastGallery style={{ width: 75, height: 75 }} resizeMode='cover'
                        source={require('../../assets/icons/cart.png')} />
                </View>
            }
            <View style={[styles.bottom, { backgroundColor: color.dark }]}>
                <View style={{ width: '50%' }}>
                    <Text style={{ color: color.light, fontWeight: '900' }}>
                        {cart.length ? `${getTotalAmount()} USD` : "CART EMPTY"}
                    </Text>
                </View>
                <View style={{ width: '50%' }}>
                    {user ?
                        <TouchableOpacity onPress={cart.length ? checkoutShopping : continueShopping}>
                            {cart.length ?
                                <Text style={{ color: color.light, fontWeight: 'bold', textAlign: 'right' }}>CHECKOUT NOW</Text> :
                                <Text style={{ color: color.light, fontWeight: 'bold', textAlign: 'right' }}>CONTINUE SHOPPING</Text>
                            }
                        </TouchableOpacity> :
                        <TouchableOpacity onPress={redirectToLogin}>
                            <Text style={{ color: color.light, fontWeight: 'bold', textAlign: 'right' }}>LOGIN</Text>
                        </TouchableOpacity>
                    }
                </View>
            </View>
        </View >
    );
}

export default Cart;