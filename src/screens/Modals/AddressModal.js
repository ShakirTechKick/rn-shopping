import React from 'react';
import { View, Text, StyleSheet, TextInput, useWindowDimensions } from "react-native";
import { Icon, Button } from 'native-base';
import MapView, { Marker, AnimatedRegion, MapViewAnimated } from "react-native-maps";
import useThemeColor from '../../helpers/useThemeColor';
import { TouchableOpacity, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import Geocoding from "react-native-geocoding";
import Geolocation from 'react-native-geolocation-service';
import { Snackbar, Dialog, Portal } from 'react-native-paper';
import LoadingDialog from '../../components/LoadingDialog';
import { useStoreActions, useStoreState } from 'easy-peasy';

Geocoding.init("AIzaSyCbXlPMYb9wm2ZAtxlns3yuCmOTTzkEpj0");

const styles = StyleSheet.create({
    container: { flex: 1 },
    map: { flex: 1 },
    bar: {
        width: '100%',
        position: 'absolute', padding: 10
    },
    back: {
        width: 40, height: 40,
        borderRadius: 50, justifyContent: 'center',
        alignItems: 'center'
    },
    menulist: {
        position: 'absolute', right: 10, top: 10
    },
    menu: {
        width: 40, height: 40,
        borderRadius: 50, justifyContent: 'center',
        alignItems: 'center'
    },
    block: {
        flexDirection: 'row',
        marginVertical: 20,
        borderRadius: 20, alignItems: 'center',
        justifyContent: 'space-between'
    },
    save: {
        width: 35, height: 35, marginHorizontal: 5,
        borderRadius: 50, justifyContent: 'center',
        alignItems: 'center', flexDirection: 'row'
    },
    current: {
        position: 'absolute', bottom: 100, right: 25
    },
    button: {
        width: 100,
        marginLeft: 10, justifyContent: 'center',
        borderWidth: 0.5, borderRadius: 20, height: 35
    },
    form: {
        marginVertical: 10
    }
})

const AddressModal = ({ route, navigation }) => {

    const { user } = useStoreState(state => state.auth);
    const { addAddress } = useStoreActions(actions => actions.addresses);

    var map = React.useRef(null);
    const [progress, setProgress] = React.useState({ visibility: false, body: "Fetching..." });

    const [saveModal, setSaveModal] = React.useState('closed');

    var fetched = null;
    if (route.params && route.params.fetched) {
        fetched = route.params.fetched;
    }

    const { color } = useThemeColor();
    const [snack, setSnack] = React.useState({ visibility: 'closed', body: null });

    const [address, setAddress] = React.useState(null);


    const [form, setForm] = React.useState({
        name: user ? user.name : null, flat: null, landmark: null, zip: null
    });

    const [currentRegion, setCurrentRegion] = React.useState({
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
    })

    const [currentMarker, setCurrentMarker] = React.useState({
        latitude: 37.78825,
        longitude: -122.4324
    })

    const onRegionChangeMap = region => {
        setCurrentRegion(region);
    }

    const onPressBack = () => {
        navigation.pop();
    }

    const onDoublePressOnMap = ({ nativeEvent: { coordinate: coordinates } }) => {
        fetchGeoLocationFromCoordinates(coordinates);
        setCurrentMarker(coordinates);
    }

    const getRegionForCoordinates = (points) => {
        let minX, maxX, minY, maxY;

        ((point) => {
            minX = point.latitude;
            maxX = point.latitude;
            minY = point.longitude;
            maxY = point.longitude;
        })(points[0]);


        points.map((point) => {
            minX = Math.min(minX, point.latitude);
            maxX = Math.max(maxX, point.latitude);
            minY = Math.min(minY, point.longitude);
            maxY = Math.max(maxY, point.longitude);
        });

        const midX = (minX + maxX) / 2;
        const midY = (minY + maxY) / 2;
        const deltaX = (maxX - minX);
        const deltaY = (maxY - minY);

        return {
            latitude: midX,
            longitude: midY,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421
        };
    }

    const fetchGeoLocationFromAddress = (address) => {

        Geocoding.from(address)
            .then(response => {

                var coordinates = response.results[0].geometry.location;
                if (typeof map.current.animateCamera === 'function') {
                    map.current.animateCamera(
                        {
                            center: { ...{ latitude: coordinates.lat, longitude: coordinates.lng } },
                            zoom: 15
                        });
                }
                setCurrentMarker({ ...currentMarker, ...{ latitude: coordinates.lat, longitude: coordinates.lng } });

            }).catch(error => {
                setSnack({ visibility: 'open', body: 'Unable fetch exact location' });
            })

    }

    const fetchGeoLocationFromCoordinates = (coordinates) => {
        Geocoding.from(coordinates.latitude, coordinates.longitude)
            .then(response => {

                var addr = response.results[0].formatted_address;
                setAddress(addr);

            }).catch(error => {
                setSnack({ visibility: 'open', body: 'Unable fetch exact location' });
            })

    }

    const onDragEndMarker = ({ nativeEvent: { coordinate: coordinates } }) => {
        fetchGeoLocationFromCoordinates(coordinates);
        setCurrentMarker(coordinates);
    }

    const fetchMyCurrentGeoLocation = () => {
        setProgress({ ...progress, visibility: true });

        Geolocation.getCurrentPosition(
            (position) => {
                setProgress({ ...progress, visibility: false });
                fetchGeoLocationFromCoordinates(position.coords);
                if (typeof map.current.animateCamera === 'function') {
                    map.current.animateCamera(
                        {
                            center: { ...position.coords },
                            zoom: 15
                        });
                }
                setCurrentMarker({ ...position.coords });
            },
            (error) => {
                setProgress({ ...progress, visibility: false });
                setSnack({ visibility: 'open', body: error.message.toString() })
            },
            {
                enableHighAccuracy: true
            }
        )

    }

    const onPressAddressList = () => {
        navigation.navigate('AddressListView');
    }

    React.useEffect(() => {
        if (fetched) {
            fetchGeoLocationFromAddress(fetched);
            setAddress(fetched);
        }
    }, [fetched])


    const onViewGooglePlaces = () => {
        navigation.navigate('GooglePlacesView');
    }

    const cancelSaveAddress = () => {
        setSaveModal('closed');
    }

    const saveAddress = () => {
        addAddress({ ...form, location: currentRegion });
        setSnack({ visibility: 'open', body: 'Successfully Address Added' });
        setSaveModal('closed');
        setTimeout(() => {
            navigation.navigate('AddressListView');
        }, 300)
    }

    const onSaveTrigger = () => {
        if (address) {
            setForm({ ...form, address });
            setSaveModal('open');
        }
    }

    return (
        <View style={StyleSheet.absoluteFillObject}>
            <MapView
                ref={map}
                onDoublePress={onDoublePressOnMap}
                onRegionChange={onRegionChangeMap}
                initialRegion={currentRegion} style={styles.map}>
                <Marker coordinate={{ ...currentMarker }}
                    draggable={true} onDragEnd={onDragEndMarker} />
            </MapView>
            <View style={[styles.bar]}>
                <TouchableOpacity onPress={onPressBack} style={[styles.back, { backgroundColor: color.light }]}>
                    <Icon name='keyboard-arrow-left' type='MaterialIcons' />
                </TouchableOpacity>
                <View style={[styles.block, { backgroundColor: color.light }]}>
                    <TextInput caretHidden placeholder="Search Location" onFocus={onViewGooglePlaces}
                        style={{ color: color.dark, borderBottomColor: 'transparent', padding: 15, width: useWindowDimensions().width - 75, overflow: 'scroll' }} underlineColorAndroid='transparent' value={address} />
                    <TouchableOpacity onPress={onSaveTrigger} style={[styles.save, { backgroundColor: color.dark }]}>
                        <Icon name='keyboard-arrow-right' style={{ color: color.light }} type='MaterialIcons' />
                    </TouchableOpacity>
                </View>
            </View>

            <View style={styles.current}>
                <TouchableOpacity onPress={fetchMyCurrentGeoLocation} style={[styles.back, { backgroundColor: color.shades }]}>
                    <Icon name='gps-fixed' style={{ color: color.light }} type='MaterialIcons' />
                </TouchableOpacity>
            </View>
            <View style={styles.menulist}>
                <TouchableOpacity onPress={onPressAddressList} style={[styles.menu, { backgroundColor: color.light }]}>
                    <Icon name='menu' type='MaterialIcons' />
                </TouchableOpacity>
            </View>

            <Snackbar visible={snack.visibility === 'open'} onDismiss={() => setSnack({ ...snack, visibility: 'closed' })}>
                {snack.body}
            </Snackbar>
            <LoadingDialog dismissable={false} content={progress.body} progress={progress.visibility} />

            <Portal>
                <Dialog visible={saveModal === 'open'}>
                    <Dialog.Title>Save Address</Dialog.Title>
                    <Dialog.Content>
                        <View style={styles.form}>
                            <TextInput placeholder='Name' value={form.name}
                                onChangeText={(name) => setForm({ ...form, name })} />
                        </View>
                        <View style={styles.form}>
                            <TextInput placeholder='Address' value={form.address}
                                onChangeText={(address) => setForm({ ...form, address })} />
                        </View>
                        <View style={styles.form}>
                            <TextInput placeholder='Flat / House Number' value={form.flat}
                                onChangeText={(flat) => setForm({ ...form, flat })} />
                        </View>
                        <View style={styles.form}>
                            <TextInput placeholder='LandMark' value={form.landmark}
                                onChangeText={(landmark) => setForm({ ...form, landmark })} />
                        </View>
                        <View style={styles.form}>
                            <TextInput placeholder='Zip/Postal Codes' value={form.zip}
                                onChangeText={(zip) => setForm({ ...form, zip })} />
                        </View>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button transparent style={[styles.button, { borderColor: color.dark }]} onPress={cancelSaveAddress}>
                            <Text style={{ fontWeight: 'bold', textAlign: 'center' }}>Cancel</Text>
                        </Button>
                        <Button transparent style={[styles.button, { borderColor: color.dark }]} onPress={saveAddress}>
                            <Text style={{ fontWeight: 'bold', textAlign: 'center' }}>Save</Text>
                        </Button>
                    </Dialog.Actions>
                </Dialog>
            </Portal>

        </View>
    )
}

export default AddressModal
