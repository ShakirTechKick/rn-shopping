import React from 'react'
import useThemeColor from '../../helpers/useThemeColor';
import { View, Text, StyleSheet, Alert, TextInput } from 'react-native';
import { useStoreState, useStoreActions } from 'easy-peasy';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Button, Icon, Body, Title, Left, Right } from 'native-base';
import { Portal, Dialog, Snackbar } from 'react-native-paper';
import AppHeader from '../../components/AppHeader';

const styles = StyleSheet.create({
    container: { flex: 1 },
    address: {
        width: '100%',
        padding: 15,
        flexDirection: 'row',
        borderBottomWidth: 0.25,
        justifyContent: 'space-between'
    },
    button: {
        width: 100,
        marginLeft: 10, justifyContent: 'center',
        borderWidth: 0.5, borderRadius: 20, height: 35
    },
    form: {
        marginVertical: 10
    }
})

const AddressList = ({ route, navigation }) => {

    const { color } = useThemeColor();
    const { updateAddress } = useStoreActions(actions => actions.addresses);
    const [snack, setSnack] = React.useState({ visibility: 'closed', body: null });


    const [form, setForm] = React.useState({
        address: null, flat: null, landmark: null
    });

    const list = useStoreState(state => state.addresses.list);
    const { removeAddress } = useStoreActions(actions => actions.addresses);
    const [updateModal, setUpdateModal] = React.useState('closed');

    const onRemoveAddress = (address) => {
        Alert.alert("Remove Address", "Are you sure to delete address ?", [
            { text: 'No', style: 'cancel', onPress: () => null },
            {
                text: 'Yes', style: 'default', onPress: () => {
                    removeAddress(address);
                }
            }
        ])
    }

    const cancelUpdateAddress = () => {
        setUpdateModal('closed');
    }

    const onUpdateAddress = () => {
        updateAddress(form);
        setSnack({ visibility: 'open', body: 'Successfully Address Updated' });
        setUpdateModal('closed');
    }

    const onSaveTrigger = (address) => {
        setForm({ ...form, ...address });
        setUpdateModal('open');
    }

    return (
        <View style={styles.container}>
            <AppHeader {...{ route, navigation }}
                renderBodyComponent={() => (<Body><Title>Delivery Addresses</Title></Body>)}
                renderLeftComponent={() => (
                    <Left><TouchableOpacity onPress={() => navigation.pop()}>
                        <Icon name='keyboard-arrow-left' style={{ color: color.light }} type='MaterialIcons' />
                    </TouchableOpacity></Left>
                )} renderRightComponent={() => null}
            />

            {
                !!list.length && list.map(address => {
                    return (
                        <View style={[styles.address,
                        { backgroundColor: color.background, borderBottomColor: color.dark }]}>
                            <View style={{ width: '90%' }}>
                                <TouchableOpacity onPress={onSaveTrigger.bind(null, address)}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 12 }}>{address.address}</Text>
                                    <Text style={{ color: 'grey' }}>{address.flat} ({address.landmark ? address.landmark : 'No Landmark specified'})</Text>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <Button transparent onPress={onRemoveAddress.bind(null, address)}>
                                    <Icon name='cancel' type='MaterialIcons' />
                                </Button>
                            </View>
                        </View>
                    )
                })
            }
            {!list.length &&
                < View style={[StyleSheet.absoluteFillObject, { justifyContent: 'center', alignItems: 'center' }]}>
                    <Text style={{ fontWeight: 'bold', fontSize: 20 }}>No Delivery Addresses</Text>
                </View>
            }
            <Snackbar visible={snack.visibility === 'open'} onDismiss={() => setSnack({ ...snack, visibility: 'closed' })}>
                {snack.body}
            </Snackbar>
            <Portal>
                <Dialog visible={updateModal === 'open'}>
                    <Dialog.Title>Save Address</Dialog.Title>
                    <Dialog.Content>
                        <View style={styles.form}>
                            <TextInput placeholder='Name' value={form.name}
                                onChangeText={(name) => setForm({ ...form, name })} />
                        </View>
                        <View style={styles.form}>
                            <TextInput placeholder='Address' value={form.address}
                                onChangeText={(address) => setForm({ ...form, address })} />
                        </View>
                        <View style={styles.form}>
                            <TextInput placeholder='Flat / House Number' value={form.flat}
                                onChangeText={(flat) => setForm({ ...form, flat })} />
                        </View>
                        <View style={styles.form}>
                            <TextInput placeholder='LandMark' value={form.landmark}
                                onChangeText={(landmark) => setForm({ ...form, landmark })} />
                        </View>
                        <View style={styles.form}>
                            <TextInput placeholder='Zip/Postal Codes' value={form.zip}
                                onChangeText={(zip) => setForm({ ...form, zip })} />
                        </View>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button transparent style={[styles.button, { borderColor: color.dark }]} onPress={cancelUpdateAddress}>
                            <Text style={{ fontWeight: 'bold', textAlign: 'center' }}>Cancel</Text>
                        </Button>
                        <Button transparent style={[styles.button, { borderColor: color.dark }]} onPress={onUpdateAddress}>
                            <Text style={{ fontWeight: 'bold', textAlign: 'center' }}>Modify</Text>
                        </Button>
                    </Dialog.Actions>
                </Dialog>
            </Portal>
        </View >
    )
}

export default AddressList;