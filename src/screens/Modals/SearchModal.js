import React from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import AppHeader from '../../components/AppHeader';
import { Left, Icon, Body, Title, Right, Button } from 'native-base';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import Product from '../../components/Product';
import Picker from "react-native-picker-select";
import { useStoreState, useStoreActions } from 'easy-peasy';
import useThemeColor from '../../helpers/useThemeColor';
import { Portal, Dialog, List, RadioButton } from 'react-native-paper';
import moment from 'moment';
import RecyclerView from '../../components/RecyclerView';

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    content: {
        flex: 1
    },
    box: {
        width: '100%'
    },
    search: {
        borderBottomColor: 'transparent',
        padding: 0, width: '100%'
    },
    history: {
        position: 'absolute', width: '100%',
        top: 50, zIndex: 1000
    },
    row: {
        marginVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    action: {
        flex: 1, flexDirection: 'row',
        alignItems: 'center', justifyContent: 'space-between',
        padding: 10
    },
    button: {
        padding: 5,
        justifyContent: 'center'
    },
    applied: {
        width: 10, height: 10, borderRadius: 50, margin: 5,
        position: 'absolute', backgroundColor: 'red',
        justifyContent: 'center', alignItems: 'center'
    },
    dot: { backgroundColor: 'rgba(250, 250, 250, 0.95)', width: 5, height: 5 }
})

const SearchModal = (props) => {

    const filter__object = {
        visibility: 'closed', isApplied: false, sort: 'Newest', price: { min: null, max: null }
    }
    const [filter, setFilter] = React.useState(filter__object);
    const [expanded, setExpanded] = React.useState({
        pricing: false, sort: true
    });

    const applyFilter = () => {
        setFilter({ ...filter, isApplied: true, visibility: 'closed' });
    }

    const clearFiler = () => {
        setFilter({ ...filter__object });
    }


    const { color } = useThemeColor();

    const [category, setCategory] = React.useState("All");
    const [search, setSearch] = React.useState(null);

    const history = useStoreState(state => state.search.history);
    const { setHistory, deleteHistory } = useStoreActions(actions => actions.search);
    const [isHistoryShown, setHistoryShown] = React.useState(false);

    const categories = useStoreState(state => state.categories.list);
    const products = useStoreState(state => state.products.list);

    const [searchProducts, setSearchProducts] = React.useState([]);

    const onNavigateBack = () => {
        props.navigation.pop();
    }

    const getLowercaseText = (text) => {
        return (text.toLowerCase());
    }

    const onPressFilterTrigger = () => {
        setFilter({ ...filter, visibility: 'open' });
    }

    const onDismissFilter = () => {
        setFilter({ ...filter, visibility: 'closed' });
    }


    const filterBasedProducts = (items = []) => {
        var filtered__products = [];

        filtered__products = items.sort((a, b) => moment(a.createdAt).diff(b.createdAt));

        if (filter.isApplied) {
            const sort = filter.sort;

            switch (sort) {
                case 'Newest':
                    filtered__products = items.sort((a, b) => moment(a.createdAt).diff(b.createdAt));
                    break;
                case 'Oldest':
                    filtered__products = items.sort((a, b) => moment(b.createdAt).diff(a.createdAt));
                    break;
                case 'A - Z':
                    filtered__products = items.sort((a, b) => a.product > b.product);
                    break;
                case 'Z - A':
                    filtered__products = items.sort((a, b) => b.product > a.product);
                    break;
                case 'Price: High - Low':
                    filtered__products = items.sort((a, b) => b.price - a.price);
                    break;
                case 'Price: Low - High':
                    filtered__products = items.sort((a, b) => a.price - b.price);
                    break;
                default:
                    filtered__products = items.sort((a, b) => moment(a.createdAt).diff(b.createdAt));
            }

            if (filter.price.min) {
                filtered__products = filtered__products.filter(p => p.price > filter.price.min);
            }

            if (filter.price.max) {
                filtered__products = filtered__products.filter(p => p.price < filter.price.max);
            }

            return filtered__products;
        }
        return filtered__products;
    }

    React.useEffect(() => {

        if (search && search.length) {

            const s = search.toLowerCase();
            var list = [];

            if (category === 'All') {
                list = products.filter(item => getLowercaseText(item.product).indexOf(s) !== -1);
                setSearchProducts([...filterBasedProducts(list)]);
            } else {
                list = products.filter(item => getLowercaseText(item.product).indexOf(s) !== -1 && item.category_id === category);
                setSearchProducts([...filterBasedProducts(list)]);
            }

        } else {
            setSearchProducts([])
        }

    }, [search])


    const onFocusSearch = () => {
        setHistoryShown(true);
    }

    const onBlurSearch = () => {
        setHistoryShown(false);
    }

    const onPressHistory = (text) => {
        setHistoryShown(false);
        setSearch(text);
    }

    const onTextInput = event => {
        setHistoryShown(search ? false : true);
    }

    const onEndSearching = event => {
        if (!history.find(s => s === search) && search) {
            setHistory(search);
        }
    }

    const onPressClearHistory = () => {
        deleteHistory();
    }
    return (
        <View style={[styles.container]}>
            <AppHeader {...props}
                headerBackgroundColor={color.light}
                renderLeftComponent={() => (
                    <Left>
                        <TouchableOpacity onPress={onNavigateBack}>
                            <Icon name='keyboard-arrow-left' style={{ color: color.primary }} type='MaterialIcons' />
                        </TouchableOpacity>
                    </Left>
                )}
                renderBodyComponent={() => (
                    <Body style={styles.box}>
                        <TextInput
                            value={search}
                            onChangeText={(text) => setSearch(text)}
                            placeholder="Search Products"
                            onFocus={onFocusSearch}
                            onBlur={onBlurSearch} onTextInput={onTextInput}
                            onEndEditing={onEndSearching}
                            underlineColorAndroid='transparent' style={styles.search} />
                    </Body>
                )} renderRightComponent={() => (
                    <Right>
                        <TouchableOpacity onPress={onPressFilterTrigger}>
                            {filter.isApplied &&
                                <View style={styles.applied}>
                                    <View style={styles.dot}></View>
                                </View>
                            }
                            <Button transparent><Icon name='filter-list' style={{ color: color.dark }} type='MaterialIcons' /></Button>
                        </TouchableOpacity>
                    </Right>
                )}
            />

            {(!!history.length && isHistoryShown) &&
                <View style={[styles.history, { backgroundColor: color.light }]}>
                    <View style={{ flex: 1, flexDirection: 'row', paddingVertical: 10, flexWrap: 'wrap' }}>
                        {
                            history.map((keyword, index) => (
                                <TouchableOpacity onPress={onPressHistory.bind(null, keyword)} key={index} style={{ padding: 5, alignSelf: 'baseline', backgroundColor: 'rgba(100, 100, 100, 0.15)', borderRadius: 5, margin: 5 }}>
                                    <Text>{keyword}</Text>
                                </TouchableOpacity>
                            ))
                        }
                    </View>
                    <View style={{ padding: 10, backgroundColor: 'rgba(100, 100, 100, 0.5)', alignItems: 'flex-end' }}>
                        <TouchableOpacity onPress={onPressClearHistory} style={{ borderRadius: 5, width: 100, padding: 5, backgroundColor: color.dark }}>
                            <Text style={{ color: color.light, textAlign: 'center' }}>Clear</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            }
            <Picker
                value={category} placeholder={{}}
                pickerProps={{ style: { backgroundColor: color.shades } }}
                onValueChange={(value) => setCategory(value)}
                Icon={() => <Button transparent><Icon style={{ color: color.dark }} name='keyboard-arrow-down' type='MaterialIcons' /></Button>}
                items={[{ label: "All", value: "All" }, ...categories.map(c => ({ label: c.category, value: c.category_id }))]} />

            {searchProducts.length > 0 ?

                <RecyclerView products={searchProducts} navigation={props.navigation} /> :
                <View style={[styles.content, { justifyContent: 'center', alignItems: 'center' }]}>
                    <Text>No Results Found !</Text>
                </View>
            }

            <Portal>
                <Dialog visible={filter.visibility === 'open'} onDismiss={onDismissFilter}>
                    <Dialog.Content>
                        <List.Section>
                            <List.Accordion
                                title="Sort" expanded={expanded.sort} style={{ backgroundColor: color.background }}
                                onPress={() => setExpanded({ ...expanded, pricing: false, sort: !expanded.sort })}>
                                <RadioButton.Group
                                    onValueChange={sort => setFilter({ ...filter, sort })} value={filter.sort}>
                                    <View style={[styles.row, { justifyContent: 'flex-start' }]}>
                                        <RadioButton value="Newest" theme={{ colors: { accent: color.primary } }} />
                                        <Text>Newest</Text>
                                    </View>
                                    <View style={[styles.row, { justifyContent: 'flex-start' }]}>
                                        <RadioButton value="A - Z" theme={{ colors: { accent: color.primary } }} />
                                        <Text>A - Z</Text>
                                    </View>
                                    <View style={[styles.row, { justifyContent: 'flex-start' }]}>
                                        <RadioButton value="Z - A" theme={{ colors: { accent: color.primary } }} />
                                        <Text>Z - A</Text>
                                    </View>
                                    <View style={[styles.row, { justifyContent: 'flex-start' }]}>
                                        <RadioButton value="Price: High - Low" theme={{ colors: { accent: color.primary } }} />
                                        <Text>Price: High - Low</Text>
                                    </View>
                                    <View style={[styles.row, { justifyContent: 'flex-start' }]}>
                                        <RadioButton value="Price: Low - High" theme={{ colors: { accent: color.primary } }} />
                                        <Text>Price: Low - High</Text>
                                    </View>
                                </RadioButton.Group>
                            </List.Accordion>
                            <List.Accordion
                                title="Pricing" expanded={expanded.pricing} style={{ backgroundColor: color.background }}
                                onPress={() => setExpanded({ ...expanded, sort: false, pricing: !expanded.pricing })}>
                                <View style={[styles.row, { justifyContent: 'flex-start' }]}>
                                    <Text>Min</Text>
                                    <TextInput value={filter.price.min} onChangeText={min => setFilter({ ...filter, price: { ...filter.price, min } })}
                                        placeholder='Min Amount' style={{ width: 200, borderBottomColor: 'transparent', padding: 10 }}
                                        underlineColorAndroid='transparent' keyboardType='decimal-pad' />
                                </View>
                                <View style={[styles.row, { justifyContent: 'flex-start' }]}>
                                    <Text>Max</Text>
                                    <TextInput value={filter.price.max} onChangeText={max => setFilter({ ...filter, price: { ...filter.price, max } })}
                                        placeholder='Max Amount' style={{ width: 200, borderBottomColor: 'transparent', padding: 10 }}
                                        underlineColorAndroid='transparent' keyboardType='decimal-pad' />
                                </View>
                            </List.Accordion>
                        </List.Section>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <View style={styles.action}>
                            <Button transparent style={[styles.button]} onPress={clearFiler}>
                                <Text style={{ color: 'red', fontWeight: 'bold' }}>Clear</Text>
                            </Button>
                            <Button transparent style={[styles.button]} onPress={applyFilter}>
                                <Text style={{ color: 'green', fontWeight: 'bold' }}>Apply</Text>
                            </Button>
                        </View>
                    </Dialog.Actions>
                </Dialog>
            </Portal>
        </View>
    )
}

export default SearchModal;
