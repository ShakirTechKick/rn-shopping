import React from 'react';
import { View, StyleSheet } from 'react-native';
import { GooglePlacesAutocomplete as GooglePlacesAutocompleteView } from "react-native-google-places-autocomplete";
import useThemeColor from '../../helpers/useThemeColor';

const GooglePlaces = ({ navigation }) => {

    const { color } = useThemeColor();

    const onPressLocation = (data, details) => {
        navigation.navigate("AddressView", { fetched: data.description });
    }

    return (
        <View style={StyleSheet.absoluteFillObject}>
            <GooglePlacesAutocompleteView
                autoFocus
                placeholder="Search Delivery Address"
                placeholderTextColor={color.dark}
                query={{
                    key: 'AIzaSyCbXlPMYb9wm2ZAtxlns3yuCmOTTzkEpj0',
                    language: 'en',
                }}
                styles={{
                    textInputContainer: {
                        backgroundColor: color.background,
                        borderTopWidth: 0, borderBottomWidth: 0
                    },
                    textInput: { height: 50 },
                    listView: { marginTop: 15 },
                    poweredContainer: { backgroundColor: color.shades }
                }}
                onPress={onPressLocation}
            />
        </View>
    )
}

export default GooglePlaces
