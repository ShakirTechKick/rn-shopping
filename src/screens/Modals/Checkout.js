import React from 'react';
import { View, Text, StyleSheet, ImageBackground } from 'react-native';
import useThemeColor from '../../helpers/useThemeColor';
import AppHeader from '../../components/AppHeader';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import { Title, Left, Icon, Body, Right, Grid, Col, Row } from 'native-base';
import { Button, IconButton, RadioButton, Snackbar } from "react-native-paper";
import { useStoreState, useStoreActions } from 'easy-peasy';
import LoadingDialog from '../../components/LoadingDialog';
import moment from 'moment';
import Keygen from 'unique-keygen';
import { Request } from '../../services/Request';

const styles = StyleSheet.create({
    container: { flex: 1 },
    content: {
        flex: 1
    },
    section: {
        margin: 10,
        borderColor: 'rgba(50, 50, 50, 0.5)',
        borderWidth: 0.5
    },
    header: {
        backgroundColor: 'rgba(50, 50, 50, 0.25)',
        padding: 10
    },
    heading: {
        fontWeight: 'bold'
    },
    view: {
        padding: 10
    },
    row: {
        marginVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    bottom: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        padding: 15
    },
    block: {
        margin: 10
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomWidth: 0.5,
        padding: 10
    },
    body: {
        padding: 10
    },
    button: {
        padding: 10,
        borderRadius: 5
    }
})

const Checkout = ({ route, navigation }) => {

    const [snack, setSnack] = React.useState({ visibility: 'closed', body: null });
    const [progress, setProgress] = React.useState({ visibility: false, body: "Placing Order..." });



    const user = useStoreState(state => state.auth.user);
    const config = useStoreState(state => state.settings.config);
    const categories = useStoreState(state => state.categories.list);
    const addresses = useStoreState(state => state.addresses.list);

    const { emptyCart } = useStoreActions(actions => actions.cart);
    const { addOrder } = useStoreActions(actions => actions.orders);

    const products = route.params.products;
    const subtotal = route.params.total;

    const shippingMethod = () => {

        let shipping_method = 'Product wise';

        if (config) {
            const { business } = config;

            if (business.find(setting => setting.type === 'shipping_cost_type')) {
                shipping_method = business.find(setting => setting.type === 'shipping_cost_type').value;
            }
        }

        return (
            (
                shipping_method[0].toUpperCase()
                + shipping_method.substr(1, shipping_method.length)
                + ' shipping cost'
            ).replace(/\_/g, ' ')
        );

    }


    const getShippingCostForProduct = (product, quantity) => {

        var shipping = 0;

        if (config) {

            const { business } = config;

            if (business.find(setting => setting.type === 'shipping_cost_type')
                && business.find(setting => setting.type === 'shipping_cost_type').value === 'produce_wise') {
                shipping = parseFloat(product.shipping_cost) * quantity;
            }

        } else {
            shipping = parseFloat(product.shipping_cost) * quantity;
        }

        if (isNaN(shipping)) {
            return 0;
        }

        return shipping.toFixed(2);

    }

    const getTaxCostForProduct = (product, quantity) => {
        var tax = 0;

        if (product.tax_type === 'percent') {

            tax = (product.price * (parseFloat(product.tax) / 100)) * quantity;

        } else {
            tax = parseFloat(product.tax) * quantity;
        }
        if (isNaN(tax)) {
            return 0;
        }
        return tax.toFixed(2);
    }

    const getDiscountForProduct = (product, quantity) => {
        var discount = 0;

        if (product.discount_type === 'percent') {

            discount = (product.price * (parseFloat(product.discount) / 100)) * quantity;

        } else {
            discount = parseFloat(product.discount) * quantity;
        }

        if (isNaN(discount)) {
            return 0;
        }

        return discount.toFixed(2);

    }



    const getTotalTaxAmount = () => {
        var amount = 0;

        amount = products.reduce((tax, product) => (
            tax + parseFloat(getTaxCostForProduct(product, product.quantity))
        ), 0)

        return amount.toFixed(2);
    }


    const getTotalDiscountAmount = () => {
        var amount = 0;

        amount = products.reduce((discount, product) => (
            discount + parseFloat(getDiscountForProduct(product, product.quantity))
        ), 0)

        return amount;
    }


    const getTotalShippingAmount = () => {

        var amount = 0;

        if (config) {

            const { business } = config;

            if (business.find(setting => setting.type === 'shipping_cost_type')
                && business.find(setting => setting.type === 'shipping_cost_type').value === 'produce_wise') {

                amount = business.find(setting => setting.type === 'shipping_cost') ?
                    parseFloat(business.find(setting => setting.type === 'shipping_cost')) : 0;
            }

        } else {
            amount = products.reduce((shipping, product) => (
                shipping + parseFloat(getShippingCostForProduct(product, product.quantity))
            ), 0)
        }


        return amount;
    }

    const getGrandTotal = () => {
        var grandtotal = 0;

        grandtotal += parseFloat(subtotal);
        grandtotal += parseFloat(getTotalShippingAmount());
        grandtotal += parseFloat(getTotalTaxAmount());
        grandtotal -= parseFloat(getTotalDiscountAmount());

        return grandtotal.toFixed(2);
    }


    const [order, setOrder] = React.useState({
        products, subtotal: subtotal,
        shipping_method: shippingMethod(),
        shipping: getTotalShippingAmount(),
        tax: getTotalTaxAmount(),
        discount: getTotalDiscountAmount(),
        grandtotal: getGrandTotal(),
        payment_method: "cash_on_delivery",
        address: null, status: 'ONGOING',
        tracking: [
            { activity: 'Order Placed', timestamp: moment(), status: 'ONGOING' }
        ],
        createdAt: new Date().toISOString()
    });

    const { color } = useThemeColor();

    const onNavigateBack = () => {
        navigation.pop();
    }

    const onPlaceOrder = () => {
        if (order.address) {
            setProgress({ ...progress, visibility: true });

            var product_details = {};

            const address = addresses.find(a => a.address_id == order.address);

            order.products.forEach(product => {
                const key = Keygen(16);
                product_details[key] = {
                    id: product.product_id,
                    qty: product.quantity,
                    option: {},
                    price: product.price,
                    name: product.product,
                    shipping: product.shipping_cost,
                    tax: product.shipping,
                    coupon: "", rowid: "", subtotal: product.price * product.quantity
                }

                if (product.colors && product.colors.selection) {
                    product_details[key].option['color'] = { title: 'Color', value: product.colors.selection.split('-').pop() };
                }
                if (product.selections) {
                    product.selections.forEach((selection, index) => {
                        product_details[key].option[`choice_${index}`] = { title: selection.name, value: selection.current.split('-').pop() };
                    })
                }

                product_details[key].option = JSON.stringify(product_details[key].option);

            })

            const request = {
                user_id: user.user_id,
                langlat: "", product_details,
                payment_type: "cash_on_delivery",
                shipping_address: {
                    firstname: address.name, lastname: "", address1: `${address.address}, Flat: ${address.flat}, Landmark: ${address.landmark}`, "address2": "",
                    zip: address.zip, email: user.email, phone: user.phone, langlat: `${address.latitude},${address.longitude}`, payment_type: order.payment_method
                }
            }

            Request.post('orders', request)
                .then(response => {
                    if (response.status === 200) {

                        const { data: rorder } = response.data;

                        addOrder({
                            ...order, order_id: rorder['sale_id'], ...rorder
                        });
                        emptyCart();
                        setSnack({ visibility: 'open', body: 'Successfully Ordered' })

                        setTimeout(() => {
                            navigation.navigate('Orders')
                        }, 300)

                    } else {
                        setSnack({ visibility: 'open', body: 'Error creating the order' });
                    }
                }).catch(error => {

                    setSnack({ visibility: 'open', body: 'Error creating the order' });

                }).finally(() => {
                    setProgress({ ...progress, visibility: false });
                })

        } else {
            setSnack({ visibility: 'open', body: 'Please select the delivery address' });
        }
    }


    const onPressAddAddress = () => {
        navigation.navigate('AddressModal');
    }

    return (
        <View style={[styles.container, { backgroundColor: color.background }]}>
            <AppHeader {...{ route, navigation }}
                headerBackgroundColor={color.light}
                renderLeftComponent={() => (
                    <Left>
                        <TouchableOpacity onPress={onNavigateBack}>
                            <Icon name='keyboard-arrow-left' style={{ color: color.primary }} type='MaterialIcons' />
                        </TouchableOpacity>
                    </Left>
                )}
                renderBodyComponent={() => (<Body><Title style={{ color: color.primary }}>Checkout</Title></Body>)}
                renderRightComponent={() => (<Right></Right>)}
            />

            <ScrollView style={[styles.content, { backgroundColor: color.background }]}>
                <View style={styles.section}>
                    <View style={[styles.header, { borderBottomColor: color.dark }]}>
                        <Text style={styles.heading}>Select Payment Method</Text>
                    </View>
                    <View style={styles.view}>
                        <RadioButton.Group
                            onValueChange={payment_method => setOrder({ ...order, payment_method })} value={order.payment_method}>
                            <View style={[styles.row, { justifyContent: 'flex-start' }]}>
                                <RadioButton value="cash_on_delivery" theme={{ colors: { accent: color.primary } }} />
                                <Text>CASH ON DELIVERY</Text>
                            </View>
                            <View style={[styles.row, { justifyContent: 'flex-start' }]}>
                                <RadioButton value="card" theme={{ colors: { accent: color.primary } }} />
                                <Text>CARD PAYMENT</Text>
                            </View>
                        </RadioButton.Group>
                    </View>
                </View>
                <View style={styles.section}>
                    <View style={[styles.header, { borderBottomColor: color.dark }]}>
                        <Text style={styles.heading}>Shipping Method</Text>
                    </View>
                    <View style={styles.view}>
                        <Text>{order.shipping_method}</Text>
                    </View>
                </View>
                <View style={styles.section}>
                    <View style={[styles.header, { borderBottomColor: color.dark }]}>
                        <Text style={styles.heading}>Select Delivery Address</Text>

                        <TouchableOpacity onPress={onPressAddAddress}>
                            <Icon name='add-circle-outline' color={color.primary} type='MaterialIcons'/>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.view}>
                        <RadioButton.Group
                            onValueChange={addr => setOrder({ ...order, address: addr })} value={order.address}>
                            {
                                addresses.map(addr => {
                                    return (
                                        <View style={[styles.row, { justifyContent: 'flex-start' }]} key={addr.address_id}>
                                            <RadioButton value={`${addr.address_id}`} theme={{ colors: { accent: color.primary } }} />
                                            <View>
                                                <Text style={{ fontWeight: 'bold', fontSize: 12, width: '75%' }}>{addr.address}</Text>
                                                <Text style={{ color: 'grey' }}>{addr.flat} ({addr.landmark ? addr.landmark : 'No LandMarks'})</Text>
                                            </View>
                                        </View>
                                    )
                                })
                            }
                        </RadioButton.Group>
                    </View>
                </View>

                <View style={styles.section}>
                    <View style={[styles.header, { borderBottomColor: color.dark }]}>
                        <Text style={styles.heading}>Products</Text>
                    </View>
                    <View style={[styles.view, { padding: 0 }]}>

                        {
                            order.products.map((product, index) => (
                                <View style={[styles.block, { backgroundColor: color.light }]}>
                                    <View style={[styles.header, { borderBottomColor: color.dark }]}>
                                        <ImageBackground
                                            resizeMode='cover'
                                            style={{ width: 100, height: 50, marginEnd: 5 }}
                                            source={{ uri: product.images[0] }} />
                                        <View>
                                            <Text style={{ fontWeight: 'bold' }}>{product.product}</Text>
                                            <Text style={{ color: 'grey' }}>
                                                {categories.find(c => c.category_id === product.category_id) ?
                                                    categories.find(c => c.category_id === product.category_id).category : null}
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={styles.body}>
                                        <Grid>
                                            <Row style={{ marginVertical: 5 }}>
                                                <Col><Text style={{ fontWeight: 'bold' }}>Product Name</Text></Col>
                                                <Col><Text style={{ textAlign: 'right' }}>{product.product}</Text></Col>
                                            </Row>
                                            <Row style={{ marginVertical: 5 }}>
                                                <Col><Text style={{ fontWeight: 'bold' }}>Price</Text></Col>
                                                <Col><Text style={{ textAlign: 'right' }}>{product.price}</Text></Col>
                                            </Row>
                                            <Row style={{ marginVertical: 5 }}>
                                                <Col><Text style={{ fontWeight: 'bold' }}>Quantity</Text></Col>
                                                <Col><Text style={{ textAlign: 'right' }}>{product.quantity}</Text></Col>
                                            </Row>
                                            <Row style={{ marginVertical: 5 }}>
                                                <Col><Text style={{ fontWeight: 'bold' }}>Shipping</Text></Col>
                                                <Col><Text style={{ textAlign: 'right' }}>{getShippingCostForProduct(product, product.quantity)}</Text></Col>
                                            </Row>
                                            <Row style={{ marginVertical: 5 }}>
                                                <Col><Text style={{ fontWeight: 'bold' }}>Tax</Text></Col>
                                                <Col><Text style={{ textAlign: 'right' }}>{getTaxCostForProduct(product, product.quantity)}</Text></Col>
                                            </Row>
                                            <Row style={{ marginVertical: 5 }}>
                                                <Col><Text style={{ fontWeight: 'bold' }}>Discount</Text></Col>
                                                <Col><Text style={{ textAlign: 'right' }}>{getDiscountForProduct(product, product.quantity)}</Text></Col>
                                            </Row>
                                            {(product.colors && product.colors.selection) &&
                                                <Row style={{ marginVertical: 5 }}>
                                                    <Col><Text style={{ fontWeight: 'bold' }}>Color</Text></Col>
                                                    <Col><View style={{
                                                        width: 15, height: 15, alignSelf: 'flex-end', borderRadius: 2,
                                                        backgroundColor: product.colors.selection.split('-').pop()
                                                    }}></View></Col>
                                                </Row>
                                            }
                                            {
                                                product.selections.map((selection, index) => (
                                                    <Row style={{ marginVertical: 5 }}>
                                                        <Col><Text style={{ fontWeight: 'bold' }}>{selection.name}</Text></Col>
                                                        <Col>
                                                            <Text style={{ textAlign: 'right' }}>
                                                                {selection.current.split('-').pop()}
                                                                {!!selection.values.find(period => period.value === selection.current.split('-').pop()).additional &&
                                                                    (`(+${selection.values.find(period => period.value === selection.current.split('-').pop()).additional})`)}
                                                            </Text>
                                                        </Col>
                                                    </Row>
                                                ))
                                            }
                                        </Grid>
                                    </View>
                                </View>
                            ))
                        }

                    </View>
                </View>

                <View style={styles.section}>
                    <View style={[styles.header, { borderBottomColor: color.dark }]}>
                        <Text style={styles.heading}>Sub Total</Text>
                    </View>
                    <View style={styles.view}>
                        <View style={styles.row}>
                            <Text>Sub Total</Text>
                            <Text style={{ textAlign: 'right' }}>{order.subtotal}</Text>
                        </View>

                        <View style={styles.row}>
                            <Text>Tax</Text>
                            <Text style={{ textAlign: 'right' }}>{order.tax}</Text>
                        </View>

                        <View style={styles.row}>
                            <Text>Shipping</Text>
                            <Text style={{ textAlign: 'right' }}>{order.shipping}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text>Discount</Text>
                            <Text style={{ textAlign: 'right' }}>{order.discount}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text>Total</Text>
                            <Text style={{ textAlign: 'right' }}>{order.grandtotal}</Text>
                        </View>
                    </View>
                </View>

            </ScrollView>

            <TouchableOpacity onPress={onPlaceOrder} style={[styles.bottom, { backgroundColor: 'green', justifyContent: 'center' }]}>
                <Text style={{ color: color.light, fontWeight: 'bold' }}>PLACE ORDER</Text>
            </TouchableOpacity>

            <Snackbar duration={1000} visible={snack.visibility === 'open'} onDismiss={() => setSnack({ ...snack, visibility: 'closed' })}>
                {snack.body}
            </Snackbar>
            <LoadingDialog dismissable={false} content={progress.body} progress={progress.visibility} />
        </View >
    )
}

export default Checkout;
