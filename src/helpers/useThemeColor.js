import React from 'react';
import { useStoreState } from 'easy-peasy';
import Colors from '../constants/Colors';

const useThemeColor = () => {

    const [color, setColor] = React.useState(Colors.light);
    const { current } = useStoreState(state => state.app);

    React.useEffect(() => {

        if (current === 'Light') {
            setColor(Colors.light)
        } else {
            setColor(Colors.dark);
        }

    }, [current])

    return { color };
}

export default useThemeColor;
