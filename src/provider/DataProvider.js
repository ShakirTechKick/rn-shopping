import { createStore, action, persist } from "easy-peasy";
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';

const model = {
    app: {
        loading: true,
        setLoading: action(function (state, payload) {
            state.user = payload
        }),
        current: 'Light',
        setThemeCurrent: action(function (state, payload) {
            state.current = payload
        }),
        token: null,
        setDeviceToken: action(function (state, payload) {
            state.token = payload
        })
    },
    settings: {
        config: null,
        setConfig: action(function (state, payload) {
            state.config = payload
        })
    },
    auth: {
        user: null,
        setUser: action(function (state, payload) {
            state.user = payload
        })
    },
    banners: {
        list: [],
        setList: action(function (state, payload) {
            state.list = payload
        })
    },
    categories: {
        list: [
            {
                category_id: 1, category: "Mobile Phones",
                image: "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/refurb-iphoneX-silver_FMT_WHH?wid=400&hei=400&fmt=jpeg&qlt=80&op_usm=0.5,0.5&.v=1546626276301",
                brands: [
                    { brand_id: 1, brand: "Samsung", image: "https://www.screenja.com/static/img/thumbs/logo-samsung-1-normal-636.png" },
                    { brand_id: 3, brand: "Apple", image: "https://img.wallpapersafari.com/tablet/800/1280/76/57/EnjqBb.jpg" }
                ]
            },
            {
                category_id: 2, category: "Shoes",
                image: "http://www.pngmart.com/files/1/Nike-Shoes-Transparent-Background.png",
                brands: [
                    { brand_id: 2, brand: "Nike", image: "https://wallpapercave.com/wp/1UKleLc.jpg" }
                ]
            }
        ],
        setList: action(function (state, payload) {
            state.list = payload
        })
    },
    brands: {
        list: [
            { brand_id: 1, brand: "Samsung", image: "https://www.screenja.com/static/img/thumbs/logo-samsung-1-normal-636.png" },
            { brand_id: 2, brand: "Nike", image: "https://wallpapercave.com/wp/1UKleLc.jpg" },
            { brand_id: 3, brand: "Apple", image: "https://img.wallpapersafari.com/tablet/800/1280/76/57/EnjqBb.jpg" },
        ],
        setList: action(function (state, payload) {
            state.list = payload
        })
    },
    sellers: {
        list: [
            {
                product_id: 1,
                category_id: 1,
                brand_id: 1,
                product: "Samsung A1",
                stock: 100,
                price: 100,
                isFavourite: false,
                isAddedToCart: false,
                images: [
                    "https://www.dhresource.com/0x0/f2/albu/g5/M00/25/B7/rBVaI1kue6OAXJ7oAAKUbsM2tM8463.jpg/original-refurbished-samsung-galaxy-on7-g6000.jpg",
                    "https://iwady.com/12545-large_default/gep.jpg"
                ],
                purchases: 245
            },
            {
                product_id: 2,
                category_id: 1,
                brand_id: 1,
                product: "Samsung A2",
                stock: 26,
                price: 100,
                isFavourite: false,
                isAddedToCart: false,
                images: [
                    "https://www.dhresource.com/0x0/f2/albu/g5/M00/25/B7/rBVaI1kue6OAXJ7oAAKUbsM2tM8463.jpg/original-refurbished-samsung-galaxy-on7-g6000.jpg",
                    "https://iwady.com/12545-large_default/gep.jpg"
                ],
                purchases: 152
            },
            {
                product_id: 3,
                category_id: 1,
                brand_id: 1,
                product: "Samsung A3",
                price: 100,
                stock: 0,
                isFavourite: false,
                isAddedToCart: false,
                images: [
                    "https://www.dhresource.com/0x0/f2/albu/g5/M00/25/B7/rBVaI1kue6OAXJ7oAAKUbsM2tM8463.jpg/original-refurbished-samsung-galaxy-on7-g6000.jpg",
                    "https://iwady.com/12545-large_default/gep.jpg"
                ],
                purchases: 100
            },
            {
                product_id: 4,
                category_id: 1,
                brand_id: 3,
                product: "IPhone 7s",
                price: 100,
                stock: 5,
                isFavourite: false,
                isAddedToCart: false,
                images: [
                    "https://www.dhresource.com/0x0/f2/albu/g5/M00/25/B7/rBVaI1kue6OAXJ7oAAKUbsM2tM8463.jpg/original-refurbished-samsung-galaxy-on7-g6000.jpg",
                    "https://iwady.com/12545-large_default/gep.jpg"
                ],
                purchases: 55
            }
        ],
        setSellerList: action(function (state, payload) {
            state.list = payload
        })
    },
    deals: {
        list: [
            {
                product_id: 1,
                category_id: 1,
                brand_id: 1,
                product: "Samsung A1",
                price: 100,
                stock: 5,
                isFavourite: false,
                isAddedToCart: false,
                images: [
                    "https://www.dhresource.com/0x0/f2/albu/g5/M00/25/B7/rBVaI1kue6OAXJ7oAAKUbsM2tM8463.jpg/original-refurbished-samsung-galaxy-on7-g6000.jpg",
                    "https://iwady.com/12545-large_default/gep.jpg"
                ],
                start: moment(new Date()),
                end: moment(new Date()).add(5, 'days'),
                offer: 50
            }
        ],
        setDealList: action(function (state, payload) {
            state.list = payload
        })
    },
    wanted: {
        list: [
            {
                product_id: 1,
                category_id: 1,
                brand_id: 1,
                product: "Samsung A1",
                price: 75,
                stock: 10,
                isFavourite: false,
                isAddedToCart: false,
                images: [
                    "https://www.dhresource.com/0x0/f2/albu/g5/M00/25/B7/rBVaI1kue6OAXJ7oAAKUbsM2tM8463.jpg/original-refurbished-samsung-galaxy-on7-g6000.jpg",
                    "https://iwady.com/12545-large_default/gep.jpg"
                ],
                likes: 200
            }
        ],
        setWantedList: action(function (state, payload) {
            state.list = payload
        })
    },
    products: {
        list: [
            // {
            //     product_id: 1,
            //     category_id: 1,
            //     brand_id: 1,
            //     product: "Samsung A1",
            //     price: 50,
            //     stock: 10,
            //     isFavourite: false,
            //     isAddedToCart: false,
            //     images: [
            //         "https://www.dhresource.com/0x0/f2/albu/g5/M00/25/B7/rBVaI1kue6OAXJ7oAAKUbsM2tM8463.jpg/original-refurbished-samsung-galaxy-on7-g6000.jpg",
            //         "https://iwady.com/12545-large_default/gep.jpg"
            //     ],
            //     selections: [
            //         { name: 'Color', values: [{ value: 'Black', additional: 0 }, { value: 'Silver', additional: 5 }], current: 'Black' },
            //         { name: 'Storage Capacity', values: [{ value: '64GB', additional: 0 }, { value: '128GB', additional: 10 }], current: '64GB' }
            //     ],
            //     additional: [], createdAt: moment('2020-08-22 15:15:40', 'YYYY-MM-DD hh:mm:ss')
            // },
            // {
            //     product_id: 2,
            //     category_id: 1,
            //     brand_id: 1,
            //     product: "Samsung A2",
            //     price: 110,
            //     stock: 29,
            //     isFavourite: false,
            //     isAddedToCart: false,
            //     images: [
            //         "https://www.dhresource.com/0x0/f2/albu/g5/M00/25/B7/rBVaI1kue6OAXJ7oAAKUbsM2tM8463.jpg/original-refurbished-samsung-galaxy-on7-g6000.jpg",
            //         "https://iwady.com/12545-large_default/gep.jpg"
            //     ],
            //     selections: [
            //         { name: 'Color', values: [{ value: 'Black', additional: 0 }, { value: 'Silver', additional: 5 }], current: 'Black' },
            //         { name: 'Storage Capacity', values: [{ value: '64GB', additional: 0 }, { value: '128GB', additional: 10 }], current: '64GB' }
            //     ],
            //     additional: [], createdAt: moment('2020-08-20 15:15:40', 'YYYY-MM-DD hh:mm:ss')
            // },
            // {
            //     product_id: 3,
            //     category_id: 1,
            //     brand_id: 3,
            //     product: "IPhone 7s",
            //     price: 150,
            //     stock: 0,
            //     isFavourite: false,
            //     isAddedToCart: false,
            //     images: [
            //         "https://icdn7.digitaltrends.com/image/digitaltrends/apple-iphone-7-iphone-7-plus-review-7-500x300-c.jpg",
            //         "https://icdn7.digitaltrends.com/image/digitaltrends/apple-iphone-7-iphone-7-plus-review-7-500x300-c.jpg"
            //     ],
            //     selections: [
            //         { name: 'Color', values: [{ value: 'Black', additional: 0 }, { value: 'Silver', additional: 5 }], current: 'Black' },
            //         { name: 'Storage Capacity', values: [{ value: '64GB', additional: 0 }, { value: '128GB', additional: 10 }], current: '64GB' }
            //     ],
            //     additional: [], createdAt: moment('2020-08-18 15:15:40', 'YYYY-MM-DD hh:mm:ss')
            // },
            // {
            //     product_id: 4,
            //     category_id: 1,
            //     brand_id: 3,
            //     product: "IPhone X",
            //     price: 200,
            //     stock: 10,
            //     isFavourite: false,
            //     isAddedToCart: false,
            //     images: [
            //         "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/refurb-iphoneX-silver_FMT_WHH?wid=400&hei=400&fmt=jpeg&qlt=80&op_usm=0.5,0.5&.v=1546626276301",
            //         "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/refurb-iphoneX-silver_FMT_WHH?wid=400&hei=400&fmt=jpeg&qlt=80&op_usm=0.5,0.5&.v=1546626276301"
            //     ],
            //     selections: [
            //         { name: 'Color', values: [{ value: 'Black', additional: 0 }, { value: 'Silver', additional: 5 }], current: 'Black' },
            //         { name: 'Storage Capacity', values: [{ value: '64GB', additional: 0 }, { value: '128GB', additional: 10 }], current: '64GB' }
            //     ],
            //     additional: [], createdAt: moment('2020-08-23 15:15:40', 'YYYY-MM-DD hh:mm:ss')
            // },
            // {
            //     product_id: 5,
            //     category_id: 2,
            //     brand_id: 2,
            //     product: "Nike Shoe V",
            //     price: 50,
            //     stock: 60,
            //     isFavourite: false,
            //     isAddedToCart: false,
            //     images: [
            //         "http://www.pngmart.com/files/1/Nike-Shoes-Transparent-Background.png",
            //         "http://www.pngmart.com/files/1/Nike-Shoes-Transparent-Background.png"
            //     ],
            //     selections: [
            //         { name: 'Color', values: [{ value: 'Black', additional: 0 }, { value: 'Silver', additional: 0 }], current: 'Black' },
            //         { name: 'Size', values: [{ value: 'S', additional: 0 }, { value: 'M', additional: 10 }, { value: 'L', additional: 15 }], current: 'S' }
            //     ],
            //     additional: [], createdAt: moment('2020-08-24 15:15:40', 'YYYY-MM-DD hh:mm:ss')
            // }
        ],
        setList: action(function (state, payload) {
            state.list = payload
        })
    },
    favourites: {
        list: [],
        addToFavourites: action(function (state, payload) {
            state.list = [...state.list, payload]
        }),
        removeFromFavourites: action(function (state, payload) {
            state.list = [...state.list.filter(f => f.product_id !== payload.product_id)]
        })
    },
    cart: {
        zoom: null,
        list: [],
        addToCart: action(function (state, payload) {
            state.zoom = Object.assign({}, { enabled: true });
            state.list = [...state.list, { ...payload, quantity: 1 }];
        }),
        removeFromCart: action(function (state, payload) {
            state.zoom = null;
            state.list = [...state.list.filter(p => p.product_id !== payload.product_id)]
        }),
        updateCartProduct: action(function (state, payload) {
            state.zoom = null;
            state.list = [...state.list.map(p => {
                if (p.product_id === payload.product_id) {
                    return payload;
                }
                return p;
            })]
        }),
        emptyCart: action(function (state, payload) {
            state.zoom = null;
            state.list = [];
        })
    },
    search: {
        history: [],
        setHistory: action(function (state, payload) {
            state.history = [...state.history, payload];
        }),
        deleteHistory: action(function (state, payload) {
            state.history = [];
        })
    },
    orders: {
        list: [],
        setOrders: action(function (state, payload) {
            state.list = [...payload];
        }),
        addOrder: action(function (state, payload) {
            state.list = [...state.list, payload];
        }),
        removeOrder: action(function (state, payload) {
            state.list = [...state.list.filter(order => order.order_id !== payload.order_id)];
        }),
        updateOrder: action(function (state, payload) {
            state.list = [...state.list.map(order => {
                if (order.order_id === payload.order_id) {
                    return { ...order, ...payload };
                }
                return order;
            })];
        }),
        clearOrders: action(function (state, payload) {
            state.list = [];
        })
    },
    addresses: {
        list: [
            // { address_id: 1, flat: "No 151/2", landmark: "Near Post Office", address: "Kaduwela Road, Oruwela, Athurugiruya." }
        ],
        addAddress: action(function (state, payload) {
            state.list = [...state.list, {
                address_id: state.list.length ? state.list[state.list.length - 1].address_id + 1 : 0,
                ...payload
            }];
        }),
        removeAddress: action(function (state, payload) {
            state.list = [...state.list.filter(a => a.address_id !== payload.address_id)];
        }),
        updateAddress: action(function (state, payload) {
            state.list = [...state.list.map(address => {
                if (address.address_id === payload.address_id) {
                    return payload;
                }
                return address;
            })];
        }),
        clearAddresses: action(function (state, payload) {
            state.list = [];
        })
    }
}

const storage = {
    async getItem(key) {
        return JSON.parse(await AsyncStorage.getItem(key))
    },
    async setItem(key, data) {
        AsyncStorage.setItem(key, JSON.stringify(data))
    },
    async removeItem(key) {
        AsyncStorage.removeItem(key)
    }
}

const DataProvider = createStore(persist(model, { storage }));
// const DataProvider = createStore(model);

export default DataProvider;