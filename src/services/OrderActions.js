import { Request } from "./Request";
import DataProvider from '../provider/DataProvider';

const {
    orders: { updateOrder }
} = DataProvider.getActions();


export const Actions = {
    'ORDER:STATUS': 'ORDER_STATUS'
}

export const functions = {
    status: async (payload) => {

        const { list: orders } = DataProvider.getState().orders;

        const order = orders.find(o => o.order_id === payload.sale_id);

        let status = 'ONGOING';

        switch (payload.status) {
            case 'delivered':
                status = 'COMPLETED';
                break;
            case 'cancel':
                status = 'CANCELLED';
                break;
            default: status = 'ONGOING';
        }

        if (order) {
            updateOrder({
                ...order, tracking: [...order.tracking,
                { activity: payload.message, timestamp: (new Date()).toISOString(), status }
                ]
            });
        }
    }
}