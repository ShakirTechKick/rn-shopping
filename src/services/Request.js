import Axios from 'axios';
const BASE_URL = 'https://ecommerce.mabtechno.com/api/';
const CancelToken = Axios.CancelToken;

export const SourceRequest = CancelToken.source();

export const Request = Axios.create({
    baseURL: BASE_URL,
    timeout: 5000,
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    cancelToken: SourceRequest.token
})

export const FormRequest = Axios.create({
    baseURL: BASE_URL,
    timeout: 5000,
    headers: {
        Accept: '*/*',
        'Content-Type': 'multipart/form-data'
    },
    cancelToken: SourceRequest.token
})

