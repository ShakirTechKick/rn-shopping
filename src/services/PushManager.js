import Messaging from '@react-native-firebase/messaging';
import { DeviceEventEmitter, PushNotificationIOS } from 'react-native';
import PushNotification from 'react-native-push-notification';
import DataProvider from '../provider/DataProvider';
import { Actions, functions } from './OrderActions';

const {
    app: { setDeviceToken }

} = DataProvider.getActions();


const createNotification = ({ title, body,
    vibrate = true, playSound = true }) => {

    const notification = {
        title, message: body, vibrate, playSound,
        priority: 'max'
    }

    PushNotification.localNotification(notification);
}

export const RunPushNotificationManager = () => {

    Messaging().getToken()
        .then(token => {
            setDeviceToken(token)
        })
        .catch(error => console.log(error))

    Messaging().onTokenRefresh(token => {
        setDeviceToken(token)
    })


    Messaging().setBackgroundMessageHandler(async ({ data }) => {

        const { payload } = JSON.parse(data.data);

        if (payload) {

            switch (payload.type) {
                case Actions['ORDER:ACCEPTED']:
                    functions.accepted(payload);
                    createNotification({
                        title: 'Order is accepted',
                        body: 'Please follow your order'
                    })
                    break;
                default: null
            }
        }

    })


    Messaging().onMessage(({ data }) => {

        const { payload } = JSON.parse(data.data);

        if (payload) {

            switch (payload.type) {
                case Actions['ORDER:STATUS']:
                    functions.status(payload);
                    createNotification({
                        title: payload.message, body: 'Please follow your order'
                    })
                    break;
                default: null
            }
        }

    })

    Messaging().getInitialNotification()
        .then(message => { }).catch(error => { })
}