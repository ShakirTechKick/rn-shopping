import { Request } from './Request';
import DataProvider from '../provider/DataProvider';

const STATIC = 'https://ecommerce.mabtechno.com/uploads/';

const {
    settings: { setConfig: setConfigurations },
    banners: { setList: setBanners },
    brands: { setList: setBrands },
    categories: { setList: setCategories },
    products: { setList: setProducts }
} = DataProvider.getActions();


export const FetchDataFromServer = () => {

    Request.get('banners')
        .then(response => {
            if (response.status === 200) {
                setBanners(response.data.map((banner, index) => ({ id: `${index}`, url: banner.replace("\\", "").trim() })));
            }
        }).catch(error => { })

    Request.get('setting')
        .then(response => {
            if (response.status === 200) {
                setConfigurations(response.data);
            }
        }).catch(error => { })


    Request.get('products')
        .then(response => {
            if (response.status === 200) {

                const products = [...response.data.map(product => {

                    const options = product.options ?
                        JSON.parse(product.options) : [];

                    product.product = product.title;
                    product.brand_id = product.brand;
                    product.category_id = product.category;
                    product.subcategory_id = product.sub_category;
                    product.price = parseFloat(product.sale_price);
                    product.selections = Array.isArray(options) ? options.map(selection => {
                        return {
                            name: selection.title, values: [...selection.option.map(option => (
                                { value: option, additional: 0 }
                            ))], current: selection.option.length ? selection.option[0] : null
                        }
                    }) : []

                    product.additional = [];
                    product.stock = parseInt(product.current_stock);

                    return product;
                })];

                setProducts([...products])
            }
        }).catch(error => {
            console.log(error)
        })


    Request.get('brands')
        .then(response => {
            if (response.status === 200) {
                setBrands([...response.data.map(b => {
                    b.brand = b.name;
                    b.image = `${STATIC}brand_image/${b.logo}`;
                    return b;
                })])
            }
        }).catch(error => { })

    Request.get('categories')
        .then(response => {
            if (response.status === 200) {

                const brands = DataProvider.getState().brands.list;

                setCategories([...response.data.map(category => {
                    category.category = category.category_name;
                    category.image = `${STATIC}category_image/${category.banner}`;
                    category.brands = category.data_brands.split(';;;;;;').map(b => {
                        return brands.find(brand => brand.brand_id === b.split(':::')[0])
                    })
                    return category;
                })])
            }
        }).catch(error => { })


}