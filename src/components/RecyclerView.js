import React from 'react';
import { StyleSheet, View, Text, Dimensions } from 'react-native';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import Product from './Product';

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center'
    }
})

class RecyclerView extends React.PureComponent {

    state = {
        renderCount: 50, columns: 2,
        layout: { ...Dimensions.get('window') }
    }

    renderProduct = ({ item, index }) => {

        const { width, height } = this.state.layout;
        const columns = this.state.columns;

        let extra = null;
        const { kind, full } = this.props;
        if (kind) {
            switch (kind) {
                case 'deals':
                    extra = { type: 'deal', start: item.start, end: item.end, offer: item.offer };
                    break;
                case 'seller':
                    extra = { type: 'seller', purchases: item.purchases };
                    break;
                default:
                    extra = { type: 'wanted', likes: item.likes };
            }
        }

        return (<Product styling={full ? { width: (width / columns) - 20 } : {}}
            navigation={this.props.navigation} key={item.product_id}
            product={item} extra={extra} />);

    }


    onEndReached() {
        this.setState({
            renderCount: this.state.renderCount + 50
        })
    }

    onLayoutList() {

        const { width, height } = Dimensions.get('window');

        let columns = 2;

        if (width > height) {
            columns = columns + (Math.floor(width / height) + 1);
        }
        this.setState({ columns, layout: { width, height } });
    }

    render() {
        const { products } = this.props;
        const { renderCount, columns } = this.state;

        return (
            <View onLayout={this.onLayoutList.bind(this)} style={styles.container}>
                <FlatList
                    key={`key-${columns}-list`}
                    data={products.filter((p, index) => index < renderCount)}
                    renderItem={this.renderProduct.bind(this)}
                    initialNumToRender={10} numColumns={columns}
                    onEndReached={this.onEndReached.bind(this)}
                    snapToAlignment='end'
                    getItemLayout={(data, index) => (
                        { index, length: 250, offset: 250 }
                    )}
                    removeClippedSubviews
                    onEndReachedThreshold={500}
                />
            </View>
        )
    }


}

export default RecyclerView;
