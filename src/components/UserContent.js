import React, { Component } from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Colors from '../constants/Colors';
import Strings from '../constants/Strings';
import useThemeColor from '../helpers/useThemeColor';

const styles = StyleSheet.create({
    content: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 25
    },
    avatar: {
        width: 75,
        height: 75,
        borderRadius: 50
    }
})

const UserContent = ({user = null, action}) => {

    const {color} = useThemeColor();

    return (
        <TouchableOpacity onPress={action} style={[styles.content, {backgroundColor: color.shades}]}>
            {user ?
                <React.Fragment>
                    <Image style={[styles.avatar]} source={{ uri: user?.avatar }} />
                    <Text style={{ fontWeight: 'bold' }}>{user?.username}</Text>
                    <Text style={{ color: 'grey', fontSize: 12 }}>{user?.phone}</Text>
                </React.Fragment> :
                <View>
                    <Text style={{
                        fontWeight: 'bold',
                        alignSelf: 'center', fontSize: 20,
                        color: color.primary
                    }}>{Strings.en['app']}</Text>
                    <Text style={{ color: 'grey', fontSize: 12 }}>Please Login To Make Orders !</Text>
                </View>
            }
        </TouchableOpacity>
    );
}

UserContent.propTypes = {

};

export default UserContent;