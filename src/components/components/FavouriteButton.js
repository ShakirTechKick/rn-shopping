import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import PropTypes from 'prop-types';
import { Icon } from 'native-base';

class FavouriteButton extends React.PureComponent {

    static propTypes = {
        isFavourite: PropTypes.bool.isRequired,
        color: PropTypes.object.isRequired
    }


    render() {
        const { isFavourite, color, onRemoveFromFavourites, onAddToFavourites } = this.props;
        return (
            <React.Fragment>
                {
                    isFavourite ?
                        <TouchableOpacity onPress={onRemoveFromFavourites}>
                            <Icon name='heart' style={{ color: 'red' }} type='EvilIcons' />
                        </TouchableOpacity> :
                        <TouchableOpacity onPress={onAddToFavourites}>
                            <Icon name='heart' style={{ color: 'grey' }} type='EvilIcons' />
                        </TouchableOpacity>
                }

            </React.Fragment>
        )
    }

}


export default FavouriteButton;