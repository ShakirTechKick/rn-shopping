import { Col, Row } from 'native-base';
import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { RadioButton } from 'react-native-paper';

const styles = StyleSheet.create({
    row: {
        marginEnd: 5
    }
})

class SelectionPicker extends React.Component {


    shouldComponentUpdate(props, states) {
        const { current, onValueChangeSelection } = this.props;

        return (
            current !== props.current || onValueChangeSelection !== props.onValueChangeSelection
        );
    }

    render() {

        const { color, current, selection, onValueChangeSelection, product } = this.props;

        return (
            <React.Fragment>
                <Row>
                    <Col>
                        <Text style={{ fontWeight: 'bold' }}>{selection.name}</Text>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <RadioButton.Group
                            onValueChange={onValueChangeSelection}
                            value={current}>
                            <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                                {
                                    selection.values.map(period => (
                                        <View style={[styles.row, { flexDirection: 'row', alignItems: 'center' }]} key={period.value}>
                                            <RadioButton value={`${product.product_id}-${selection.name}-${period.value}`} theme={{ colors: { accent: color.primary } }} />
                                            <View><Text>{period.value}</Text></View>
                                        </View>
                                    ))
                                }
                            </View>
                        </RadioButton.Group>
                    </Col>
                </Row>
            </React.Fragment>
        )
    }
}

export default SelectionPicker;
