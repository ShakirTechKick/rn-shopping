import React from 'react';
import { TouchableOpacity, Text , StyleSheet} from 'react-native';
import PropTypes from 'prop-types';


const styles = StyleSheet.create({
    cart: {
        padding: 5,
        borderRadius: 5
    }
})

class CartButton extends React.PureComponent {

    static propTypes = {
        isAddedToCart: PropTypes.bool.isRequired,
        color: PropTypes.object.isRequired
    }


    render() {
        const { isAddedToCart, color, onRemoveFromCart, onAddToCart } = this.props;
        return (
            <React.Fragment>
                {
                    isAddedToCart ?
                        <TouchableOpacity onPress={onRemoveFromCart} style={[styles.cart, { backgroundColor: 'red' }]}>
                            <Text style={{ color: color.light, fontSize: 12, fontWeight: 'bold' }}>REMOVE CART</Text>
                        </TouchableOpacity> :
                        <TouchableOpacity onPress={onAddToCart} style={[styles.cart, { backgroundColor: 'green' }]}>
                            <Text style={{ color: color.light, fontSize: 12, fontWeight: 'bold' }}>ADD TO CART</Text>
                        </TouchableOpacity>
                }
            </React.Fragment>
        )
    }

}


export default CartButton;