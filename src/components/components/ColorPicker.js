import { Col, Row } from 'native-base';
import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { RadioButton } from 'react-native-paper';

const styles = StyleSheet.create({
    item: {
        marginVertical: 5
    },
    row: {
        marginEnd: 5
    }
})

class ColorPicker extends React.Component {


    shouldComponentUpdate(props, states) {
        const { current , onValueChangeColor} = this.props;

        return (current !== props.current || onValueChangeColor !== props.onValueChangeColor);
    }

    render() {

        const { color, current, onValueChangeColor, product } = this.props;

        return (
            <Row style={styles.item}>
                <Col>
                    <Text style={{ fontWeight: 'bold' }}>Select Color</Text>
                    <RadioButton.Group
                        onValueChange={onValueChangeColor}
                        value={current}>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                            {
                                [...JSON.parse(product.color)].map(c => (
                                    <View style={[styles.row, { justifyContent: 'flex-start', flexDirection: 'row', alignItems: 'center' }]} key={c}>
                                        <RadioButton value={`${product.product_id}-${c}`} theme={{ colors: { accent: c } }} />
                                        <View style={{
                                            width: 25, height: 25, backgroundColor: c,
                                            margin: 5, borderColor: color.dark, borderWidth: 2
                                        }} />
                                    </View>
                                ))
                            }
                        </View>
                    </RadioButton.Group>
                </Col>
            </Row>
        )
    }
}

export default ColorPicker;
