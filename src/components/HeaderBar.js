import React from 'react';
import { StyleSheet, View, Text } from 'react-native'
import useThemeColor from '../helpers/useThemeColor';

const styles = StyleSheet.create({
    header: {
        padding: 15
    }
})

function HeaderBar({text}) {
    const { color } = useThemeColor();
    return (
        <View style={[styles.header, { backgroundColor: color.light }]}>
            <Text style={[{ fontSize: 15, fontWeight: 'bold' }]}>{text}</Text>
        </View>
    )
}

export default HeaderBar;
