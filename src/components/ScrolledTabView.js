import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { FlatList, TouchableOpacity, TouchableNativeFeedback } from 'react-native-gesture-handler';
import Colors from '../constants/Colors';
import PropTypes from 'prop-types';
import useThemeColor from '../helpers/useThemeColor';

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        flex: 1,
        height: 50,
        paddingHorizontal: 15,
        justifyContent: 'center',
        backgroundColor: 'rgba(250, 250, 250, 1)'
    },
    headerText: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    tab: {
        flex: 1,
        padding: 15
    },
    tabText: {
        alignSelf: 'baseline',
        fontWeight: 'bold'
    },
    selected: {
        borderBottomWidth: 2
    },
    selectedText: {},
    content: {
        flex: 1
    }
})

const ScrolledTabView = React.memo((props) => {

    const { color } = useThemeColor();

    const [current, setCurrent] = React.useState(props.current ? props.current : 0);

    const onSelectCurrent = index => setCurrent(index);

    const renderHeaderTabs = ({ item, index }) => {
        return (
            <TouchableOpacity key={item} onPress={onSelectCurrent.bind(null, index)} style={[
                styles.tab, { backgroundColor: color.light }, props.tabStyles ? props.tabStyles : {},
                current === index && (props.selectedStyles ? { ...styles.selected, borderBottomColor: color.dark, ...props.selectedStyles } : { ...styles.selected, borderBottomColor: color.dark })
            ]}>
                <Text style={[
                    styles.tabText, { color: color.dark }, props.tabTextStyles ? props.tabTextStyles : {},
                    current === index && (props.selectedTextStyles ? { ...styles.selectedText, color: color.primary, ...props.selectedTextStyles } : { ...styles.selectedText, color: color.primary })
                ]}>{item}</Text>
            </TouchableOpacity>
        )
    }

    return (
        <View style={styles.container}>
            {props.header &&
                <View style={[styles.header, props.headerStyles ? props.headerStyles : {}]}>
                    <Text style={[styles.headerText, { color: color.dark }, props.headerTextStyles ? props.headerTextStyles : {}]}>{props.header}</Text>
                </View>
            }
            <View style={{ flex: 1, backgroundColor: color.light }}>
                {
                    (props.tabs && Array.isArray(props.tabs)) &&
                    <FlatList
                        horizontal
                        data={props.tabs}
                        renderItem={renderHeaderTabs}
                        key={(item, index) => item}
                    />
                }
            </View>
            <View style={styles.content}>{props.renderContent(props.data[current])}</View>
        </View>
    );
});

ScrolledTabView.protoTypes = {
    header: PropTypes.string,
    data: PropTypes.arrayOf(PropTypes.array).isRequired,
    renderContent: PropTypes.func.isRequired
}

export default ScrolledTabView;