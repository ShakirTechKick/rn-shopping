import React, { Component } from 'react';
import { View, StyleSheet, ImageBackground, Text, useWindowDimensions, Dimensions, ToastAndroid } from 'react-native';
import Colors from '../constants/Colors';
import { TouchableOpacity, TouchableNativeFeedback } from 'react-native-gesture-handler';
import { Icon } from 'native-base';
import { useStoreActions, useStoreState } from 'easy-peasy';
import useThemeColor from '../helpers/useThemeColor';
import moment from 'moment';

import FastGallery from 'react-native-fast-image';
import CartButton from './components/CartButton';
import FavouriteButton from './components/FavouriteButton';

const styles = StyleSheet.create({
    view: {
        padding: 5, borderRadius: 5,
        alignSelf: 'center', margin: 5
    },
    product: {
        width: '100%',
        height: 100
    },
    header: {
        alignSelf: 'center',
        fontSize: 12,
        marginVertical: 5
    },
    prices: {
        position: 'absolute', padding: 2,
        zIndex: 999, backgroundColor: 'rgb(235, 33, 40)',
        borderBottomRightRadius: 10,
        shadowColor: 'rgb(250, 250, 250)', elevation: 5
    },
    buttons: {
        marginVertical: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    extra: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopColor: 'rgba(50, 50, 50, 0.25)',
        borderTopWidth: 0.5, paddingTop: 10
    },
    dot: {
        width: 5, height: 5, borderRadius: 50, backgroundColor: 'rgb(250, 250, 250)'
    }
})

const Product = ({ navigation, product, extra = null, styling = {} }) => {

    const { color } = useThemeColor();

    const favourites = useStoreState(state => state.favourites.list);
    const cart = useStoreState(state => state.cart.list);
    const { addToFavourites, removeFromFavourites } = useStoreActions(actions => actions.favourites);
    const { addToCart, removeFromCart } = useStoreActions(actions => actions.cart);

    const isAddedToCart = cart.find(c => c.product_id === product.product_id);
    const isFavourite = favourites.find(f => f.product_id === product.product_id);

    const onAddToCart = React.useCallback(() => {
        addToCart(product);

        ToastAndroid.showWithGravity("Product added to cart", ToastAndroid.SHORT, ToastAndroid.CENTER);
    }, [])

    const onRemoveFromCart =  React.useCallback(() => {
        removeFromCart(product);

        ToastAndroid.showWithGravity("Product removed from cart", ToastAndroid.SHORT, ToastAndroid.CENTER);
    }, []);


    const onAddToFavourites =  React.useCallback(() => {
        addToFavourites(product);

        ToastAndroid.showWithGravity("Product added to favourites", ToastAndroid.SHORT, ToastAndroid.CENTER);
    }, []);

    const onRemoveFromFavourites =  React.useCallback(() => {
        removeFromFavourites(product);
        ToastAndroid.showWithGravity("Product removed from favourites", ToastAndroid.SHORT, ToastAndroid.CENTER);
    }, []);

    const onViewProduct = () => {
        navigation.navigate('ProductView', { product });
    }

    const renderExtra = () => {
        switch (extra.type) {
            case 'deal':
                return (
                    <View>
                        <Text style={{ fontWeight: 'bold', color: 'rgb(235, 33, 40)' }}>
                            Valid upto {moment(extra.end).format('YYYY MMM DD hh:mm:ss a')}
                        </Text>
                    </View>
                )
            case 'wanted':
                return (
                    <View>
                        <Text style={{ fontWeight: 'bold' }}>Likes : {extra.likes}</Text>
                    </View>
                )
            default:
                return (
                    <View>
                        <Text style={{ fontWeight: 'bold' }}>Purchased : {extra.purchases} times.</Text>
                    </View>
                )
        }
    }


    return (
        <View style={[styles.view,
        { width: 170, backgroundColor: color.light, height: extra ? 225 : 200 }, styling]} onPress={onViewProduct}>
            <View style={styles.prices}>
                <View style={styles.dot}></View>
                <Text style={[{ color: color.light }, (extra && extra.offer) && { textDecorationLine: 'line-through', textDecorationStyle: 'solid' }]}>{product.price} USD</Text>
                {(extra && extra.offer) &&
                    <Text style={{ color: color.light }}>{extra.offer} USD</Text>
                }
            </View>
            <TouchableOpacity onPress={onViewProduct}>
                <FastGallery 
                resizeMode={"cover"} fallback style={styles.product}
                source={{uri: product.thumbs ? product.thumbs[0] : product.images[0]}}/>
                <Text style={styles.header}>{product.product}</Text>
            </TouchableOpacity>
            <View style={styles.buttons}>
                {product.stock > 0 ?
                    <CartButton isAddedToCart={!!isAddedToCart} color={color} onAddToCart={onAddToCart} onRemoveFromCart={onRemoveFromCart} /> :
                    <View>
                        <Text style={{ color: 'red', fontWeight: 'bold' }}>Out Of Stock</Text>
                    </View>
                }

                <FavouriteButton isFavourite={!!isFavourite} color={color} onAddToFavourites={onAddToFavourites} onRemoveFromFavourites={onRemoveFromFavourites}/>

            </View>
            {
                extra &&
                <View style={styles.extra}>
                    {renderExtra()}
                </View>
            }
        </View>
    );
}

export default React.memo(Product);