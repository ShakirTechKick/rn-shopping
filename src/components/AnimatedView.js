import React from 'react';
import Animated, { Easing } from 'react-native-reanimated';
import { PanResponder } from 'react-native';

const AnimatedView = ({ children, styles = {}, action = () => null, max = 1.5 }) => {

    const [zoom, setZoom] = React.useState(new Animated.Value(1));

    const TouchPanResponder = PanResponder.create({
        onStartShouldSetPanResponder: () => true,
        onMoveShouldSetPanResponder: () => true,
        onPanResponderGrant: (event, gesture) => {
            Animated.timing(zoom, {
                toValue: new Animated.Value(max),
                duration: 100,
                easing: Easing.linear,
                useNativeDriver: true
            }).start();
        },
        onPanResponderRelease: (event, gesture) => {
            Animated.timing(zoom, {
                toValue: new Animated.Value(1),
                duration: 100, easing: Easing.cubic,
                useNativeDriver: true
            }).start();

            setTimeout(() => {
                action();
            }, 300)
        }
    });


    return (
        <Animated.View  {...TouchPanResponder.panHandlers}
            style={[styles, { transform: [{ scale: zoom }] }]}>
            {children}
        </Animated.View>
    )
}

export default AnimatedView
