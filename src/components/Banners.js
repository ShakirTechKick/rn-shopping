import { useStoreState } from 'easy-peasy';
import React from 'react';
import { StyleSheet, View, Image, Text, Dimensions } from 'react-native';
import FastImageGallery from 'react-native-fast-image';
import { FlatList } from 'react-native-gesture-handler';
import { ActivityIndicator } from 'react-native-paper';
import Swiper from 'react-native-swiper';
import useThemeColor from '../helpers/useThemeColor';

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 175,
        justifyContent: 'center',
        alignItems: 'center'
    },
    banner: {
        width: '100%',
        height: 200
    }
})

function Banners() {


    const { color } = useThemeColor();
    const { list: banners } = useStoreState(state => state.banners);

    // const banners = [
    //     {id: 1, url: "https://picsum.photos/536/354"},
    //     {id: 2, url: "https://picsum.photos/id/237/536/354"},
    //     {id: 3, url: "https://picsum.photos/seed/picsum/536/354"},
    //     {id: 4, url: "https://picsum.photos/id/1084/536/354?grayscale"},
    //     {id: 5, url: "https://picsum.photos/id/1060/536/354?blur=2"},
    // ]

    return (
        <View style={[styles.container]}>
            {(banners && banners.length) ?
                <Swiper horizontal activeDotColor={color.shades}>
                    {banners.map(banner => (
                        <FastImageGallery key={banner.id} style={styles.banner}
                            source={{ uri: banner.url }} />
                    ))
                    }
                </Swiper>
                :
                <ActivityIndicator color={color.primary} size={30} />
            }
        </View>
    )
}

export default Banners
