import React from 'react'
import { Dialog, ActivityIndicator } from 'react-native-paper'
import { View, Text, StyleSheet } from 'react-native';
import useThemeColor from '../helpers/useThemeColor';

const styles = StyleSheet.create({
    content: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    text: { fontWeight: 'bold', paddingHorizontal: 10, fontSize: 20 }
})

function LoadingDialog({ progress = false, dismissable = true, content = "Please wait ..." }) {
    
    const {color} = useThemeColor();

    return (
        <Dialog dismissable={dismissable} visible={progress} >
            <Dialog.Content>
                <View style={styles.content}>
                    <ActivityIndicator color={color.shades} />
                    <Text style={styles.text}>{content}</Text>
                </View>
            </Dialog.Content>
        </Dialog>
    )
}

export default LoadingDialog
