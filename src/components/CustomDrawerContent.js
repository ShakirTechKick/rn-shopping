import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Image, Text } from 'react-native';
import { useStoreState, useStoreActions } from 'easy-peasy';
import Strings from '../constants/Strings';
import UserContent from './UserContent';
import { DrawerContent } from '@react-navigation/drawer';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icon, Button } from 'native-base';
import useThemeColor from '../helpers/useThemeColor';

const styles = StyleSheet.create({})

const CustomDrawerContent = (props) => {

    const { color } = useThemeColor();

    const user = useStoreState(state => state.auth.user);
    const setUser = useStoreActions(actions => actions.auth.setUser);
    const list = useStoreState(state => state.addresses.list);

    const onLoginView = () => {
        props.navigation.navigate('AuthScreen');
    }

    const onAddressView = () => {
        props.navigation.navigate('AddressModal');
    }

    const onLogoutUser = () => {
        setUser(null);
    }

    const action = () => {
        if (user) {
            props.navigation.navigate('AccountView');
        }
    }

    return (
        <>
            <UserContent user={user} action={action} />
            <DrawerContent {...props} />
            {
                user ?
                    <>
                        <TouchableOpacity onPress={onAddressView} style={{ flexDirection: 'row', marginHorizontal: 15, borderWidth: 2, borderRadius: 5, borderColor: color.primary, justifyContent: 'center', padding: 10, alignItems: 'center' }}>
                            <Icon name='map' type='MaterialIcons' style={{ paddingHorizontal: 10, color: color.shades }} />
                            <Text style={{ color: color.primary, fontWeight: 'bold' }}>Add Delivery Address {!!list.length && `(${list.length})`} </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={onLogoutUser} style={{ marginHorizontal: 15, borderWidth: 2, marginVertical: 5, borderRadius: 5, borderColor: color.primary, justifyContent: 'center', padding: 10, alignItems: 'center' }}>
                            <Text style={{ color: color.primary, fontWeight: 'bold' }}>Logout</Text>
                        </TouchableOpacity>
                    </> :
                    <TouchableOpacity onPress={onLoginView} style={{ margin: 15, borderWidth: 2, borderRadius: 5, borderColor: color.primary, justifyContent: 'center', padding: 10, alignItems: 'center' }}>
                        <Text style={{ color: color.primary, fontWeight: 'bold' }}>Login</Text>
                    </TouchableOpacity>
            }

        </>
    );
}

CustomDrawerContent.propTypes = {

};

export default CustomDrawerContent;