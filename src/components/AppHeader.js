import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Header, Left, Body, Right, Title, Icon, Button } from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Strings from '../constants/Strings';
import { PanResponder, Animated, StyleSheet, View, Text, Image } from "react-native";
import { Easing } from 'react-native-reanimated';
import { useStoreState } from 'easy-peasy';
import useThemeColor from '../helpers/useThemeColor';
import Swiper from 'react-native-swiper';

const styles = StyleSheet.create({
    button: {
        padding: 10
    },
    badge_cart: {
        position: 'absolute', left: 25,
        width: 20, height: 20,
        justifyContent: 'center', alignItems: 'center',
        borderRadius: 50
    }
})

const AppHeader = ({ transparent = null, translucent = null,
    headerBackgroundColor, statusBarColor, extraStyles = {},
    children, renderLeftComponent = null, renderBodyComponent = null, renderRightComponent = null, route, navigation }) => {

    const { color } = useThemeColor();

    const list = useStoreState(state => state.cart.list);
    const zoom = useStoreState(state => state.cart.zoom);

    const [cartZoom, setCartZoom] = React.useState(new Animated.Value(1));

    const TouchPanResponder = PanResponder.create({
        onStartShouldSetPanResponder: () => true,
        onMoveShouldSetPanResponder: () => true,
        onPanResponderGrant: (event, gesture) => {
            Animated.timing(cartZoom, {
                toValue: new Animated.Value(2),
                duration: 100,
                easing: Easing.linear,
                useNativeDriver: true
            }).start();
        },
        onPanResponderRelease: (event, gesture) => {
            Animated.spring(cartZoom, {
                toValue: new Animated.Value(1),
                mass: 2, useNativeDriver: true
            }).start();

            setTimeout(() => {
                navigation.navigate('CartModal');
            }, 300)
        }
    });


    const animateCartButton = () => {
        Animated.sequence([
            Animated.timing(cartZoom, {
                toValue: 2,
                duration: 200,
                useNativeDriver: true
            }),
            Animated.timing(cartZoom, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true
            })
        ]).start();
    }


    React.useEffect(() => {
        if (zoom) {
            animateCartButton();
        }
    }, [zoom])

    const openSearchMdal = () => {
        navigation.navigate('SearchModal');
    }

    return (
        <Header transparent={!!transparent} translucent={!!translucent}
            style={[{ backgroundColor: headerBackgroundColor ? headerBackgroundColor : color.primary, zIndex: 999 }, extraStyles]} iosBarStyle='light-content' androidStatusBarColor={statusBarColor ? statusBarColor : color.primary}>
            {
                renderLeftComponent ?
                    renderLeftComponent() : <Left>
                        <TouchableOpacity onPress={() => navigation.openDrawer()}>
                            <Icon name='menu' type='MaterialIcons' style={{ color: color.light }} />
                        </TouchableOpacity>
                    </Left>
            }

            {
                renderBodyComponent ? renderBodyComponent() :
                    <Body>
                        <Title>{Strings.en['app']}</Title>
                    </Body>
            }

            {
                renderRightComponent ? renderRightComponent() :
                    <Right>
                        <TouchableOpacity style={[styles.button]} onPress={openSearchMdal}>
                            <Icon name='search' type='MaterialIcons' style={{ color: color.light }} />
                        </TouchableOpacity>
                        <Animated.View {...TouchPanResponder.panHandlers}
                            style={[styles.button, {
                                transform: [
                                    { scale: cartZoom }
                                ]
                            }]}>
                            <Icon name='shopping-cart' type='MaterialIcons' style={{ color: color.light }} />
                            {!!list.length &&
                                <View style={[styles.badge_cart, { backgroundColor: color.light }]}>
                                    <Text style={{ color: 'grey', fontSize: 10 }}>{list.length}</Text>
                                </View>
                            }
                        </Animated.View>
                    </Right>
            }
        </Header>

    )
}


export default AppHeader;