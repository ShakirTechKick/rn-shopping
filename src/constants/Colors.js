export default {
    light: {
        primary: 'rgba(0, 189, 195,1)',
        light: 'rgba(250, 250, 250, 1)',
        shades: 'rgba(0, 189, 195,0.25)',
        dark: 'rgba(50, 50, 50, 1)',
        background: 'rgba(150, 150, 150, 0.05)'
    },
    dark: {
        primary: 'rgba(30, 31, 30,1)',
        light: 'rgba(50, 50, 50, 1)',
        shades: 'rgba(30, 31, 30,0.25)',
        dark: 'rgba(250, 250, 250, 1)',
        background: 'rgba(50, 50, 50, 0.05)'
    }
}