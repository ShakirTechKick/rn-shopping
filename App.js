/**
 * User App (Ecommerce Application)
 * https://www.mabtechno.com
 * Dev: M.A.H.M. Shakir Hassan
 * @format
 * @flow strict-local
 */

import React from 'react';
import { ActivityIndicator, View, Text, Button, BackHandler }
  from 'react-native'; /** React Native Dependant Components **/
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator, DrawerContent } from '@react-navigation/drawer';

import { Provider as PaperProvider } from 'react-native-paper';

import SplashScreen from './src/screens/SplashScreen';
import HomeScreen from './src/screens/HomeScreen';
import CategoryScreen from './src/screens/CategoryScreen';
import ShopScreen from './src/screens/ShopScreen';
import MyFavouritesScreen from './src/screens/MyFavouritesScreen';
import NewsScreen from './src/screens/NewsScreen';
import OrderScreen from './src/screens/OrderScreen';
import AboutScreen from './src/screens/AboutScreen';
import SettingsScreen from './src/screens/SettingsScreen';

import CustomDrawerContent from './src/components/CustomDrawerContent';
import { useStoreRehydrated } from 'easy-peasy';
import ProductView from './src/screens/ProductView';
import CartModal from './src/screens/Modals/CartModal';
import SearchModal from './src/screens/Modals/SearchModal';
import LoginScreen from './src/screens/LoginScreen';
import VerifyScreen from './src/screens/VerifyScreen';
import RegisterScreen from './src/screens/RegisterScreen';
import OrderDetailScreen from './src/screens/OrderDetailScreen';
import useThemeColor from './src/helpers/useThemeColor';
import Checkout from './src/screens/Modals/Checkout';
import AddressModal from './src/screens/Modals/AddressModal';
import GooglePlaces from './src/screens/Modals/GooglePlaces';
import AddressList from './src/screens/Modals/AddressList';
import OrderTracking from './src/screens/OrderTracking';
import Account from './src/screens/Account';

import Messaging from '@react-native-firebase/messaging';
import PushNotification from 'react-native-push-notification';

const RootStack = createStackNavigator();
const AuthStack = createStackNavigator();
const MainDrawer = createDrawerNavigator();


const HomeStack = () => {
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator headerMode='none' initialRouteName='HomeView'>
      <Stack.Screen name='HomeView' component={HomeScreen} />
    </Stack.Navigator>
  )
}

const OrderStack = () => {
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator headerMode='none' initialRouteName='OrderView'>
      <Stack.Screen name='OrderView' component={OrderScreen} />
      <Stack.Screen name='OrderDetailsView' component={OrderDetailScreen} />
      <Stack.Screen name='OrderTrackView' component={OrderTracking} />
    </Stack.Navigator>
  )
}


const AppScreen = () => {

  const { color } = useThemeColor();

  return (
    <MainDrawer.Navigator openByDefault={false} headerMode='none' mode='card'
      backBehavior='history'
      drawerContentOptions={{
        activeBackgroundColor: color.primary,
        activeTintColor: color.light
      }} drawerContent={props => (<CustomDrawerContent {...props} />)}>
      <MainDrawer.Screen name='Home' options={{ drawerLabel: 'Home' }} component={HomeStack} />
      <MainDrawer.Screen name='Categories' options={{ drawerLabel: 'Categories' }} component={CategoryScreen} />
      <MainDrawer.Screen name='MyFavouries' options={{ drawerLabel: 'My Favourites' }} component={MyFavouritesScreen} />
      <MainDrawer.Screen name='Orders' options={{ drawerLabel: 'My Orders' }} component={OrderStack} />
      <MainDrawer.Screen name='About' options={{ drawerLabel: 'About' }} component={AboutScreen} />
      <MainDrawer.Screen name='Settings' options={{ drawerLabel: 'Settings' }} component={SettingsScreen} />

    </MainDrawer.Navigator>
  )
}


const AuthScreen = (props) => {

  return (
    <AuthStack.Navigator headerMode='none' mode='modal'>
      <AuthStack.Screen name='LoginView' component={LoginScreen.bind(null, { ...props })} />
      <AuthStack.Screen name='VerifyView' component={VerifyScreen} />
      <AuthStack.Screen name='RegisterView' component={RegisterScreen} />
    </AuthStack.Navigator>
  )
}



const CartStack = () => {
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator headerMode='none' initialRouteName='OrderView'>
      <Stack.Screen name='CartView' component={CartModal} />
      <Stack.Screen name='CheckOutView' component={Checkout} />
    </Stack.Navigator>
  )
}

const AddressStack = () => {
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator headerMode='none' initialRouteName='OrderView'>
      <Stack.Screen name='AddressView' component={AddressModal} />
      <Stack.Screen name='GooglePlacesView' component={GooglePlaces} />
      <Stack.Screen name='AddressListView' component={AddressList} />
    </Stack.Navigator>
  )
}


const App = () => {

  const { color } = useThemeColor();

  return (
    <PaperProvider>
      <NavigationContainer
        fallback={() => <ActivityIndicator size='large' color={color.primary} />}>
        <RootStack.Navigator headerMode='none' mode='modal'>
          {!useStoreRehydrated() &&
            <RootStack.Screen name='SplashScreen' component={SplashScreen} />}
          <RootStack.Screen name='AppScreen' component={AppScreen} />
          <RootStack.Screen name='AuthScreen' component={AuthScreen} />
          <RootStack.Screen name='CartModal' component={CartStack} />
          <RootStack.Screen name='SearchModal' component={SearchModal} />
          <RootStack.Screen name='AddressModal' component={AddressStack} />
          <RootStack.Screen name='ShopView' component={ShopScreen} />
          <RootStack.Screen name='ProductView' component={ProductView} />
          <RootStack.Screen name='AccountView' component={Account} />
        </RootStack.Navigator>
      </NavigationContainer>
    </PaperProvider>
  );
};


export default App;
