/**
 * @format
 */
import React from "react";
import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import { StoreProvider } from "easy-peasy";
import DataProvider from "./src/provider/DataProvider";
import { RunPushNotificationManager } from "./src/services/PushManager";

const Application = () => {
    return (
        <StoreProvider store={DataProvider}>
            <App />
        </StoreProvider>
    )
}

RunPushNotificationManager();

AppRegistry.registerComponent(appName, () => Application);
